import React, { useState, useEffect } from "react";

import * as Location from "expo-location";
import { storeData, getData } from "@modules/util";

// contexts
export default () => {
  const [isShow, setShow] = useState(false);
  const [address, setAddress] = useState(null);
  const [location, setLocation] = useState(null);

  useEffect(() => {
    (async () => {
      const location = await getData("location");
      const address = await getData("address");
      if (location) {
        setLocation(location);
        setAddress(address);
      }
    })();
  }, []);

  return { location, address, isShow, setLocation, setAddress, setShow };
};
