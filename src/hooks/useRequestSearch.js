import React, { useState, useEffect } from "react";
import { getPostsByKeyword } from "@services/post";

export default function useRequestSearch(type) {
  const [offset, setOffset] = useState(0);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isLast, setIsLast] = useState(false);
  const [keyword, setKeyword] = useState("");

  useEffect(() => {
    if (keyword) {
      setOffset(0);
      setPosts([]);
      fetchData();
    }
  }, [keyword]);

  const fetchData = async () => {
    setIsLast(false);
    setLoading(true);
    const { data } = await getPostsByKeyword({
      offset: 0,
      type,
      keyword,
    });
    setPosts(data);
    if (data.length !== 10) setIsLast(true);
    setLoading(false);
    setOffset(10);
  };

  const loadMoreData = async () => {
    if (isLast || posts.length !== offset) return;
    setOffset(offset + 10);
    try {
      setLoading(true);
      const { data } = await getPostsByKeyword({
        offset,
        type,
        keyword,
      });
      setPosts([...posts, ...data]);
      if (data.length !== 10) setIsLast(true);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  return {
    posts,
    loading,
    fetchData,
    loadMoreData,
    setKeyword,
  };
}
