import React, { useState, useEffect } from "react";

import * as Location from "expo-location";
import { storeData, getData } from "@modules/util";

export default (callback) => {
  useEffect(() => {
    const setLocation = async () => {
      const location = await getData("location");
      if (!location) {
        let { status } = await Location.requestPermissionsAsync();
        if (status !== "granted") {
          setErrorMsg("Permission to access location was denied");
        }
        let location = await Location.getCurrentPositionAsync({
          accuracy: Location.Accuracy.High,
        });
        await storeData({ key: "location", value: location.coords });
        callback();
      }
    };
    setLocation();
  }, []);
};
