import React, { useState, useEffect } from "react";
import { getPostsByDistance } from "@services/post";

export default function useRequestPost(type) {
  const [offset, setOffset] = useState(0);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isLast, setIsLast] = useState(false);
  const [sortType, setSortType] = useState("");
  const [filterType, setFilterType] = useState("");

  useEffect(() => {
    setOffset(0);
    setPosts([]);
    fetchData();
  }, [sortType, filterType]);

  const fetchData = async () => {
    setIsLast(false);
    setLoading(true);
    const { data } = await getPostsByDistance({
      offset: 0,
      type,
      sortType,
      filterType,
    });
    setPosts(data);
    if (data.length !== 10) setIsLast(true);
    setLoading(false);
    setOffset(10);
  };

  const loadMoreData = async () => {
    if (isLast || posts.length !== offset) return;
    setOffset(offset + 10);
    try {
      setLoading(true);
      const { data } = await getPostsByDistance({
        offset,
        type,
        sortType,
      });
      setPosts([...posts, ...data]);
      if (data.length !== 10) setIsLast(true);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  return {
    posts,
    loading,
    fetchData,
    loadMoreData,
    sortType,
    setSortType,
    filterType,
    setFilterType,
  };
}
