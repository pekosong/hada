import React, { useEffect } from "react";
import messaging from "@react-native-firebase/messaging";
import { useDispatch } from "react-redux";
import { setMessages } from "@stores/message";
import { getAllDocument } from "@modules/firebase";
import { setApplications } from "@stores/application";
import { setPosts } from "@stores/post";

import { getMyApplications } from "@services/application";
import { getMyPosts } from "@services/post";

export default function useMessage(user, callback, setMessage) {
  const dispatch = useDispatch();
  useEffect(() => {
    messaging().onNotificationOpenedApp(async (remoteMessage) => {
      try {
        if (!remoteMessage) return;
        const { data } = remoteMessage;
        if (data.type === "message") {
          const chatList = await getAllDocument({ id: user.id });
          dispatch(setMessages(chatList));
        }
      } catch (err) {
        console.log(err);
      }
    });
    messaging()
      .getInitialNotification()
      .then(async (remote) => {
        // console.log(data);
        // if (data.type === "message") {
        //   const chatList = await getAllDocument({ id: user.id });
        //   dispatch(setMessages(chatList));
        // }
      });
  }, []);

  useEffect(() => {
    let unsubscribe;
    if (user) {
      unsubscribe = messaging().onMessage(async (remoteMessage) => {
        try {
          if (!remoteMessage) return;
          const { notification, data } = remoteMessage;
          if (data.type === "message") {
            const chatList = await getAllDocument({ id: user.id });
            dispatch(setMessages(chatList));
          } else if (data.type === "application") {
            setMessage(notification.body);
            callback(true);
            const applications = await getMyApplications();
            dispatch(setApplications(applications));
            const posts = await getMyPosts();
            dispatch(setPosts(posts));
          }
        } catch (err) {
          console.log(err);
        }
      });
    }
    return unsubscribe;
  }, [user]);
}
