export const perCategory = {
  perDay: { text: "일당", bgColor: "#FFF1F1", textColor: "#FF6200" },
  perHour: { text: "시급", bgColor: "#EBF9EB", textColor: "#07B200" },
  perPerson: { text: "인당", bgColor: "#E6D1E7", textColor: "#DF1BED" },
  perCase: { text: "건당", bgColor: "#ECF6FE", textColor: "#1289EF" },
};
