import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import Text from "./Text";
import Modal from "./Modal";

export default ({ showModal, setShowModal, title, menuItems }) => {
  return (
    <Modal showModal={showModal} setShowModal={setShowModal}>
      <Text s2 bold margin={[0, 0, 20]}>
        {title}
      </Text>
      {menuItems.map((el) => (
        <TouchableOpacity
          key={el.text}
          onPress={() => {
            el.onPress();
            setShowModal(false);
          }}
        >
          <View style={styles.modalItem}>
            <Text>{el.text}</Text>
          </View>
        </TouchableOpacity>
      ))}
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
});
