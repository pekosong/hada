import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";

// components
import Text from "./Text";

// modules
import { getSize } from "@modules/util";

const size = getSize();

export default ({ callback, message }) => {
  useEffect(() => {
    setTimeout(() => {
      callback(false);
    }, 3000);
  }, []);
  return (
    <View style={styles.header}>
      <Text s3>{message}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    position: "absolute",
    top: 40,
    left: 30,
    width: size.width - 60,
    zIndex: 100,
    padding: 10,
    borderRadius: 2,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#ccc",
    opacity: 0.9,
  },
});
