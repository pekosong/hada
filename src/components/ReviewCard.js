import React from "react";
import { StyleSheet, View, Image } from "react-native";
import Text from "./Text";

import { AirbnbRating } from "react-native-ratings";

import { fromNow } from "@modules/util";

const avatar = require("@assets/images/avatar.png");

export default ReviewCard = ({ item }) => {
  return (
    <View style={{ paddingVertical: 10 }}>
      <View style={styles.row}>
        <Image
          style={styles.avatarImage}
          source={
            item.guest.profile ? { uri: item.guest.profile["url"] } : avatar
          }
        />
        <View style={{ marginLeft: 10 }}>
          <Text s4 margin={[0, 0, 0, 3]}>
            {item.guest.nickname ?? item.guest.name}
          </Text>
          <AirbnbRating
            showRating={false}
            count={5}
            defaultRating={item.rating}
            size={10}
          />
        </View>
        <Text g1 s5 style={{ marginLeft: "auto" }}>
          {fromNow(item.created_at)}
        </Text>
      </View>
      <Text s4 g1>
        {item.contents}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  row: { flexDirection: "row", alignItems: "center", marginBottom: 10 },
  avatarImage: { width: 40, height: 40, borderRadius: 40 },
});
