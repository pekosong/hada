import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import Modal from "react-native-modal";

const { width } = Dimensions.get("window");

export default ({ showModal, setShowModal, children, padding }) => {
  return (
    <Modal
      animationInTiming={500}
      useNativeDriver={true}
      isVisible={showModal}
      entry={"top"}
      onBackdropPress={() => setShowModal(false)}
    >
      <View style={styles.container}>
        <View style={{ padding: padding || 20 }}>{children}</View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    bottom: -20,
    position: "absolute",
    width: width,
    marginLeft: -20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
