import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import Text from "./Text";

import COLORS from "@modules/color";

const down = require("@assets/icons/down.png");

export default ({ type, onPress, text, sortType, filterType }) => {
  const types = sortType
    ? sortType.split("_")
    : filterType
    ? filterType.split("_")
    : [];
  const active = type === types[0];
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[
          styles.filterItem,
          active && {
            borderWidth: 1,
            borderColor: COLORS.main,
            backgroundColor: COLORS.lightMain,
          },
        ]}
      >
        <Text main={active} bold={active} s5 margin={[0, 5, 0, 0]}>
          {text}
        </Text>
        {!!active &&
          types.length &&
          (types[0] === "status" ? (
            <Text black bold s5 margin={[0, 5, 0, 0]}>
              {types[1] === "all"
                ? ""
                : types[1] === "finished"
                ? "미션 완료"
                : types[1] === "proceeding"
                ? "수행 중"
                : "모집 중"}
            </Text>
          ) : types[0] === "price" ? (
            <Text black bold s5 margin={[0, 5, 0, 0]}>
              {types[1] === "DESC" ? "금액 높은 순" : "금액 낮은 순"}
            </Text>
          ) : types[0] === "day" ? (
            <Text black bold s5 margin={[0, 5, 0, 0]}>
              {types[1] === "DESC" ? "최신 순" : "오래된 순"}
            </Text>
          ) : (
            <Text black bold s5 margin={[0, 5, 0, 0]}>
              {types[1] === "perHour"
                ? "시급"
                : types[1] === "perDay"
                ? "일당"
                : "건당"}
            </Text>
          ))}
        <Image source={down} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  filterItem: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 50,
    marginRight: 10,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderColor: COLORS.playGray,
    backgroundColor: COLORS.playGray,
  },
});
