import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import Text from "./Text";
import Modal from "./Modal";

const selectRoundGray = require("@assets/icons/selectRoundGray.png");
const selectRoundYellow = require("@assets/icons/selectRoundYellow.png");

export default ({
  showModal,
  setShowModal,
  modalType,
  sortType,
  setSortType,
  filterType,
  setFilterType,
}) => {
  return (
    <Modal showModal={showModal} setShowModal={setShowModal}>
      {modalType === "price" && (
        <>
          <Text s2 bold margin={[0, 0, 20]}>
            금액
          </Text>
          {[
            { sort: "price_DESC", text: "금액 높은 순" },
            { sort: "price_ASC", text: "금액 낮은 순" },
          ].map((el) => (
            <TouchableOpacity
              key={el.sort}
              onPress={() => {
                setFilterType("");
                setSortType(el.sort);
                setShowModal(false);
              }}
            >
              <View style={styles.modalItem}>
                <Text>{el.text}</Text>
                <Image
                  source={
                    sortType === el.sort ? selectRoundYellow : selectRoundGray
                  }
                />
              </View>
            </TouchableOpacity>
          ))}
        </>
      )}
      {modalType === "day" && (
        <>
          <Text s2 bold margin={[0, 0, 20]}>
            등록 순서
          </Text>
          {[
            { sort: "day_DESC", text: "최신 순" },
            { sort: "day_ASC", text: "오랜된 순" },
          ].map((el) => (
            <TouchableOpacity
              key={el.sort}
              onPress={() => {
                setFilterType("");
                setSortType(el.sort);
                setShowModal(false);
              }}
            >
              <View style={styles.modalItem}>
                <Text>{el.text}</Text>
                <Image
                  source={
                    sortType === el.sort ? selectRoundYellow : selectRoundGray
                  }
                />
              </View>
            </TouchableOpacity>
          ))}
        </>
      )}
      {modalType === "priceType" && (
        <>
          <Text s2 bold margin={[0, 0, 20]}>
            임금 형태
          </Text>
          {[
            { filter: "priceType_perHour", text: "시급" },
            { filter: "priceType_perDay", text: "일당" },
            { filter: "priceType_perCase", text: "건당" },
          ].map((el) => (
            <TouchableOpacity
              key={el.filter}
              onPress={() => {
                setSortType("");
                setFilterType(el.filter);
                setShowModal(false);
              }}
            >
              <View style={styles.modalItem}>
                <Text>{el.text}</Text>
                <Image
                  source={
                    filterType === el.filter
                      ? selectRoundYellow
                      : selectRoundGray
                  }
                />
              </View>
            </TouchableOpacity>
          ))}
        </>
      )}
      {modalType === "status" && (
        <>
          <Text s2 bold margin={[0, 0, 20]}>
            미션 상태
          </Text>
          {[
            { filter: "status_all", text: "전체" },
            { filter: "status_proceeding", text: "수행 중" },
            { filter: "status_recruiting", text: "모집 중" },
            { filter: "status_finished", text: "미션 완료" },
          ].map((el) => (
            <TouchableOpacity
              key={el.filter}
              onPress={() => {
                setSortType("");
                setFilterType(el.filter);
                setShowModal(false);
              }}
            >
              <View style={styles.modalItem}>
                <Text>{el.text}</Text>
                <Image
                  source={
                    filterType === el.filter
                      ? selectRoundYellow
                      : selectRoundGray
                  }
                />
              </View>
            </TouchableOpacity>
          ))}
        </>
      )}
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
});
