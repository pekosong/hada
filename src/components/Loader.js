import React from "react";
import { View } from "react-native";
import { DotsLoader } from "react-native-indicator";
import COLORS from "@modules/color";

export default () => (
  <View
    style={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#fff",
    }}
  >
    <DotsLoader color={COLORS.main} />
  </View>
);
