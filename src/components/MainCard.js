import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";

import Text from "./Text";
import {
  getData,
  covertPrice,
  fromNow,
  duration,
  getDistanceFromLatLonInKm,
} from "@modules/util";

import { useSelector } from "react-redux";
import { selectLike } from "@stores/like";
import { AntDesign, Entypo } from "@expo/vector-icons";
import { perCategory } from "@constants/category";

const hada = require("@assets/icons/app_icon.jpg");

export default ({
  item,
  onPress,
  menu,
  onMenuPress,
  applyStatus,
  HandleUpdatePostProcess,
  HandleUpdateCompleteProcess,
}) => {
  const {
    id,
    address,
    title,
    price,
    images,
    likers,
    created_at,
    priceType,
    status,
    lat,
    lng,
    applications,
  } = item;
  const [distance, setDistance] = useState(item.distance || "");

  useEffect(() => {
    if (!item.distance) {
      getData("location").then((value) => {
        const { latitude, longitude } = value;
        setDistance(getDistanceFromLatLonInKm(lat, lng, latitude, longitude));
      });
    }
  }, []);

  const { likes } = useSelector(selectLike);
  const icon = perCategory[priceType];
  return (
    <>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.container}>
          <View style={styles.image}>
            <Image
              style={styles.image}
              source={images.length !== 0 ? { uri: images[0]["url"] } : hada}
            />
            {status === "finished" && (
              <View style={styles.overlap}>
                <View style={styles.overlapCircle}>
                  <AntDesign name="check" color={COLORS.white} size={16} />
                </View>
                <Text s5 white>
                  미션완료
                </Text>
              </View>
            )}
          </View>
          <View style={styles.content}>
            <View style={styles.titleContainer}>
              <Text s4 bold lines={1} style={{ flex: 1, paddingRight: 5 }}>
                {title}
              </Text>
              {menu ? (
                <TouchableOpacity onPress={onMenuPress} style={{ padding: 5 }}>
                  <Entypo name={"dots-three-vertical"} size={14} />
                </TouchableOpacity>
              ) : (
                <Text s5 g2>
                  {fromNow(created_at)}
                </Text>
              )}
            </View>
            <Text s5 g1>
              {`${
                address && address.split(" ")[2]
                  ? address.split(" ")[2]
                  : address
              }  ${
                Number.parseFloat(distance) < 100
                  ? Number.parseFloat(distance).toFixed(1) + "km 이내"
                  : "100km 이상"
              }`}
            </Text>
            <Text s5 g1>
              {duration(item)}
            </Text>
            <View style={styles.titleContainer}>
              <View style={styles.badgeContainer}>
                <View
                  style={{
                    ...styles.icon,
                    backgroundColor: icon.bgColor,
                  }}
                >
                  <Text s5 style={{ color: icon.textColor }}>
                    {icon.text}
                  </Text>
                </View>
                <Text s5 margin={[0, 0, 0, 5]}>
                  {price === "0" ? "협의" : covertPrice(price)}
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                {menu && (
                  <View style={styles.badgeContainer}>
                    <AntDesign name={"user"} size={14} />
                    <Text s5 margin={[0, 10, 0, 5]}>
                      {applications.filter((el) => el.user).length}
                    </Text>
                  </View>
                )}
                {likers && (
                  <View style={styles.badgeContainer}>
                    <AntDesign
                      name={
                        likes.map((el) => el.id).indexOf(id) === -1
                          ? "hearto"
                          : "heart"
                      }
                      size={14}
                      color={
                        likes.map((el) => el.id).indexOf(id) === -1
                          ? COLORS.gray
                          : COLORS.danger
                      }
                    />
                    <Text s5 margin={[0, 10, 0, 5]}>
                      {likers.length}
                    </Text>
                  </View>
                )}
              </View>
              {applyStatus === "success" && (
                <Text s4 main>
                  성공
                </Text>
              )}
              {applyStatus === "apply" && <Text s4>지원 중</Text>}
              {applyStatus === "deny" && (
                <Text s4 danger>
                  실패
                </Text>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
      {menu && (HandleUpdatePostProcess || HandleUpdateCompleteProcess) && (
        <View style={styles.bottomContainer}>
          {HandleUpdatePostProcess && (
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={{ width: "100%", paddingVertical: 10 }}
                onPress={() => {
                  HandleUpdatePostProcess();
                }}
              >
                <Text center s5>
                  수행중으로 변경
                </Text>
              </TouchableOpacity>
            </View>
          )}
          {HandleUpdatePostProcess && HandleUpdateCompleteProcess && (
            <View style={styles.divider}></View>
          )}
          {HandleUpdateCompleteProcess && (
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={{ width: "100%", paddingVertical: 10 }}
                onPress={HandleUpdateCompleteProcess}
              >
                <Text center s5>
                  미션완료로 변경
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    flexDirection: "row",
    height: 100,
  },
  image: {
    width: 85,
    height: 85,
  },
  content: {
    flex: 1,
    height: 85,
    paddingLeft: 10,
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  badgeContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  icon: {
    paddingHorizontal: 8,
    borderRadius: 10,
  },
  overlap: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: COLORS.black,
    opacity: 0.6,
    alignItems: "center",
    justifyContent: "center",
  },
  overlapCircle: {
    borderWidth: 1,
    borderColor: COLORS.white,
    borderRadius: 50,
    padding: 2,
    marginBottom: 2,
  },
  bottomContainer: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: COLORS.gray,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.gray,
  },
  divider: {
    borderRightWidth: 1,
    borderRightColor: COLORS.gray,
    height: 30,
  },
});
