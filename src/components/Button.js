import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import COLORS from "@modules/color";
import Text from "@components/Text";

export default ({
  main,
  lightMain,
  text,
  style,
  gray,
  outline,
  children,
  onPress,
  textMain,
  textWhite,
  textGray,
  normal,
  white,
}) => {
  const buttonStyle = [
    styles.container,
    lightMain && styles.lightMain,
    gray && styles.gray,
    outline && styles.outline,
    main && styles.main,
    normal && styles.normal,
    white && styles.white,
    style,
  ];
  return (
    <TouchableOpacity style={buttonStyle} onPress={onPress}>
      {text && (
        <Text s4 main={textMain} g2={textGray} white={textWhite || main}>
          {text}
        </Text>
      )}
      {children && children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    paddingHorizontal: 10,
    borderRadius: 2,
    backgroundColor: COLORS.playGray,
    borderColor: COLORS.playGray,
    borderWidth: 1,
  },
  outline: {
    borderColor: COLORS.gray,
    backgroundColor: COLORS.white,
  },
  gray: {
    backgroundColor: COLORS.playGray,
    borderColor: COLORS.playGray,
  },
  lightMain: {
    backgroundColor: COLORS.lightMain,
    borderColor: COLORS.main,
  },
  main: {
    backgroundColor: COLORS.main,
    borderColor: COLORS.main,
  },
  normal: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.white,
  },
});
