import React from "react";
import { StyleSheet, TextInput } from "react-native";
import COLORS from "@modules/color";

export default ({
  placeholder,
  style,
  value,
  onChangeText,
  err,
  success,
  multiline,
  numberOfLines,
  keyboardType,
}) => {
  const inputStyle = [
    styles.container,
    err && styles.errStyle,
    success && styles.successStyle,
    multiline && {
      height: 100,
      textAlignVertical: "top",
    },
    style,
  ];
  return (
    <TextInput
      style={inputStyle}
      placeholder={placeholder}
      onChangeText={onChangeText}
      value={value}
      multiline={multiline}
      numberOfLines={numberOfLines}
      keyboardType={keyboardType}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    borderColor: COLORS.gray,
    borderWidth: 1,
    borderRadius: 2,
    paddingHorizontal: 10,
  },
  errStyle: {
    borderColor: COLORS.danger,
  },
  successStyle: {
    borderColor: COLORS.success,
  },
});
