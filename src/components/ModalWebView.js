import React from "react";
import Modal from "react-native-modal";
import { WebView } from "react-native-webview";

export default ({ showModal, setShowModal, onMessage, lat, lng }) => {
  const latitude = lat || "35.14969";
  const longitude = lng || "126.9128783";

  return (
    <Modal
      animationInTiming={500}
      useNativeDriver={true}
      isVisible={showModal}
      entry={"top"}
      onBackdropPress={() => setShowModal(false)}
      style={{ margin: 0 }}
    >
      <WebView
        scalesPageToFit={false}
        originWhitelist={["*"]}
        source={{
          uri: `https://tirrilee-team02.github.io/tirrilee-kakaomap/?lat=${latitude}&lng=${longitude}`,
          // uri: `http://peek.iptime.org:3000/?lat=${latitude}&lng=${longitude}`,
        }}
        onMessage={onMessage}
      />
    </Modal>
  );
};
