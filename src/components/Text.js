import React from "react";

import { Text } from "react-native";

// modules
import COLORS from "@modules/color";
import SIZES from "@modules/size";

export default (props) => {
  // Size
  const { s1, s2, s3, s4, s5, s6, s7 } = props;

  // Color 1
  const { main, point, black, white, success, danger } = props;

  // Color 2 : gray
  const { g1, g2, g3, g4 } = props;

  // etc
  const { bold, margin, lines, style, underline, children, center } = props;

  const handleMargin = () => {
    if (typeof margin === "number") {
      return {
        marginTop: margin,
        marginRight: margin,
        marginBottom: margin,
        marginLeft: margin,
      };
    }

    if (typeof margin === "object") {
      const marginSize = Object.keys(margin).length;
      switch (marginSize) {
        case 1:
          return {
            marginTop: margin[0],
            marginRight: margin[0],
            marginBottom: margin[0],
            marginLeft: margin[0],
          };
        case 2:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[0],
            marginLeft: margin[1],
          };
        case 3:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[2],
            marginLeft: margin[1],
          };
        default:
          return {
            marginTop: margin[0],
            marginRight: margin[1],
            marginBottom: margin[2],
            marginLeft: margin[3],
          };
      }
    }
  };

  const styles = [
    {
      fontSize: 16,
      color: COLORS.black,
      fontFamily: "NotoSansCJKkr-Regular",
      lineHeight: 16 * 1.6,
    },
    s1 && { fontSize: SIZES.s1, lineHeight: SIZES.s1 * 1.6 },
    s2 && { fontSize: SIZES.s2, lineHeight: SIZES.s2 * 1.6 },
    s3 && { fontSize: SIZES.s3, lineHeight: SIZES.s3 * 1.6 },
    s4 && { fontSize: SIZES.s4, lineHeight: SIZES.s4 * 1.6 },
    s5 && { fontSize: SIZES.s5, lineHeight: SIZES.s5 * 1.6 },
    s6 && { fontSize: SIZES.s6, lineHeight: SIZES.s6 * 1.6 },
    s7 && { fontSize: SIZES.s7, lineHeight: SIZES.s7 * 1.6 },
    main && { color: COLORS.main },
    point && { color: COLORS.point },
    black && { color: COLORS.black },
    white && { color: COLORS.white },
    g1 && { color: COLORS.darkGray },
    g2 && { color: COLORS.gray },
    g3 && { color: COLORS.lightGray },
    g4 && { color: COLORS.playGray },
    success && { color: COLORS.success },
    danger && { color: COLORS.danger },
    bold && { fontFamily: "NotoSansCJKkr-Bold" },
    center && { textAlign: "center" },
    underline && { textDecorationLine: "underline" },
    handleMargin(),
    style,
  ];
  return (
    <Text style={styles} numberOfLines={lines} ellipsizeMode="tail">
      {children}
    </Text>
  );
};
