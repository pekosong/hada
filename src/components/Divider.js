import React from "react";
import { StyleSheet, View } from "react-native";
import COLORS from "@modules/color";

export default ({ width = 10, padding = 10 }) => {
  const buttonStyle = [
    styles.divider,
    { borderBottomWidth: width },
    { paddingBottom: padding, marginBottom: padding },
  ];
  return <View style={buttonStyle}></View>;
};

const styles = StyleSheet.create({
  divider: {
    borderBottomColor: COLORS.playGray,
    borderBottomWidth: 10,
  },
});
