import React from "react";
import { StyleSheet, View, Image, Platform } from "react-native";
import Container from "@layouts/container";

import Text from "@components/Text";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useDispatch } from "react-redux";

import KakaoLogins, { KAKAO_AUTH_TYPES } from "@react-native-seoul/kakao-login";
import { appleAuth } from "@invertase/react-native-apple-authentication";

import { signIn, verifyUsername } from "@services/auth";
import { setLogin } from "@stores/auth";
import { getSize, storeData } from "@modules/util";

const title = require("@assets/images/title.png");
const appleIcon = require("@assets/icons/appleIcon.png");
const kakaoIcon = require("@assets/icons/kakaoIcon.png");

const { width } = getSize();

// contexts
export default ({ navigation }) => {
  const dispatch = useDispatch();

  const login = async ({ username, provider }) => {
    try {
      const { data } = await verifyUsername(username);
      if (data === 1) {
        const { data } = await signIn(username);
        await storeData({ key: "token", value: data.jwt });
        dispatch(setLogin());
      } else {
        navigation.navigate("Agreement", {
          username,
          provider,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleKakaoLogin = async () => {
    try {
      await KakaoLogins.login([KAKAO_AUTH_TYPES.Talk]);
      const kakao = await KakaoLogins.getProfile();
      await login({
        username: kakao.id,
        provider: "kakao",
      });
    } catch (err) {
      console.log(err);
    }
  };

  const handleAppleLogin = async () => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
      });
      await login({
        username: appleAuthRequestResponse.user,
        provider: "apple",
      });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Container>
      <View style={[styles.center, { flex: 2, paddingTop: 70 }]}>
        <Image source={title} style={{ height: 77, width: 145 }}></Image>
        <Text margin={[20, 0, 0, 0]}>내 주변 일거리 매칭 플랫폼</Text>
      </View>
      <View style={styles.center}>
        <TouchableOpacity onPress={handleKakaoLogin} style={styles.kakaoButton}>
          <Image source={kakaoIcon} style={styles.kakaoIcon}></Image>
          <Text center s4 style={{ flex: 1 }}>
            카카오 계정으로 로그인
          </Text>
        </TouchableOpacity>
        <View style={{ height: 10 }}></View>
        {Platform.OS === "ios" && (
          <TouchableOpacity
            onPress={handleAppleLogin}
            style={styles.appleButton}
          >
            <Image source={appleIcon} style={styles.appleIcon}></Image>
            <Text center s4 white style={{ flex: 1 }}>
              애플 계정으로 로그인
            </Text>
          </TouchableOpacity>
        )}
        <Text darkGray s5 margin={[50, 0, 0, 0]}>
          회원가입 없이 카카오톡 계정만으로 바로 이용이 가능합니다.
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate("Agreement")}>
          <Text darkGray s5 margin={[0, 0, 20, 0]}>
            로그인 시,{` `}
            <Text s5 underline bold>
              이용약관 및 개인정보처리방침 동의
            </Text>
            로 간주됩니다.
          </Text>
        </TouchableOpacity>
        <Text s5 gray margin={[0, 0, 100, 0]}>
          ⓒ (주)퍼니 브라운 All Rights Reserved
        </Text>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  kakaoButton: {
    height: 55,
    borderRadius: 5,
    width: width * 0.8,
    backgroundColor: "#ffe812",
    justifyContent: "flex-start",
    paddingHorizontal: 30,
    flexDirection: "row",
    alignItems: "center",
  },
  kakaoIcon: {
    width: 27,
    height: 25,
  },
  appleButton: {
    height: 55,
    borderRadius: 5,
    width: width * 0.8,
    backgroundColor: "#000",
    justifyContent: "flex-start",
    paddingHorizontal: 30,
    flexDirection: "row",
    alignItems: "center",
  },
  appleIcon: {
    width: 25,
    height: 25,
  },
});
