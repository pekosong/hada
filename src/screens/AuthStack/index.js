import React from "react";

import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import LoginScreen from "./LoginScreen";
import SignUpScreen from "./SignUpScreen";
import AgreementScreen from "./AgreementScreen";

const AuthStack = createStackNavigator();
function AuthStackNavigation() {
  return (
    <AuthStack.Navigator headerMode="none">
      <AuthStack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          ...TransitionPresets.ScaleFromCenterAndroid,
        }}
      />
      <AuthStack.Screen
        name="SignUp"
        component={SignUpScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <AuthStack.Screen
        name="Agreement"
        component={AgreementScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </AuthStack.Navigator>
  );
}

export default AuthStackNavigation;
