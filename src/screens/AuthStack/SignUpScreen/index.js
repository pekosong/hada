import React, { useEffect, useState, useReducer } from "react";
import {
  StyleSheet,
  Platform,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { Button, Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import KeyboardSpacer from "react-native-keyboard-spacer";
import auth from "@react-native-firebase/auth";
import { AntDesign } from "@expo/vector-icons";

import moment from "moment";
import COLORS from "@modules/color";
import { getSize, storeData } from "@modules/util";

import { useDispatch } from "react-redux";
import { setProfile } from "@stores/auth";
import { verifyNick, signUp } from "@services/auth";
import { addPayment } from "@services/payment";

const { width } = getSize();

const initValue = {
  birthDay: "",
  name: "",
  nameErr: "",
  phone: "",
  phoneErr: "",
  email: "",
  emailErr: "",
  nickname: "",
  nicknameErr: "",
  nicknameSuccess: "",
  gender: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "update":
      const errors = validateValue(action);
      return { ...state, [action.name]: action.value, ...errors };
    default:
      return state;
  }
};

const validateValue = (action) => {
  let errors = {};
  if (action.name === "name") {
    const korRe = /^[가-힣]+$/;
    if (!korRe.test(action.value)) {
      errors["nameErr"] = "이름을 정확히 입력하세요";
    } else if (action.value.length < 2) {
      errors["nameErr"] = "이름을 정확히 입력하세요";
    } else {
      errors["nameErr"] = "";
    }
  }
  if (action.name === "email") {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const validEmail = re.test(String(action.value).toLowerCase());
    if (!validEmail) {
      errors["emailErr"] = "이메일을 정확히 입력하세요.";
    } else {
      errors["emailErr"] = "";
    }
  }
  if (action.name === "phoneNumber") {
    const re = /^\d*\.?\d*$/;
    const validNumber = re.test(action.value);
    if (!validNumber) {
      errors["phoneErr"] = "핸드폰 번호를 정확히 입력하세요.";
    } else if (action.value.length !== 11) {
      errors["phoneErr"] = "핸드폰 번호를 정확히 입력하세요.";
    } else {
      errors["phoneErr"] = "";
    }
  }
  return errors;
};

// contexts
export default ({ route, navigation }) => {
  const authDispatch = useDispatch();
  const { username, provider } = route.params;
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const [payment, setPayment] = useState(null);

  const [state, dispatch] = useReducer(reducer, initValue);
  const {
    birthDay,
    name,
    nickname,
    phoneNumber,
    email,
    gender,
    nameErr,
    phoneErr,
    emailErr,
    nicknameErr,
    nicknameSuccess,
  } = state;

  useEffect(() => {
    auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user);
        if (Platform.OS !== "ios") setConfirm(true);
      }
    });
  }, []);

  const handleConfirm = (date) => {
    const value = moment(date).format("YYYY-MM-DD");
    handleChange({ name: "birthDay", value });
    setDatePickerVisibility(false);
  };

  const handleChange = ({ name, value }) => {
    dispatch({ type: "update", name, value });
  };

  const handlePhoneCode = async () => {
    const phone = `+82 ${phoneNumber.slice(0, 3)}-${phoneNumber.slice(
      3,
      7
    )}-${phoneNumber.slice(7, 11)}`;
    try {
      signInWithPhoneNumber(phone);
    } catch (err) {
      console.log(err);
    }
  };

  const signInWithPhoneNumber = async (phone) => {
    const confirmation = await auth().signInWithPhoneNumber(phone);
    setConfirm(confirmation);
  };

  const handleCheckNick = async () => {
    try {
      const result = await verifyNick(nickname);
      if (result.data === 1) {
        handleChange({ name: "nicknameSuccess", value: "" });
        handleChange({
          name: "nicknameErr",
          value: "이미 존재하는 닉네임입니다.",
        });
      } else {
        handleChange({ name: "nicknameSuccess", value: "멋진 닉네임이네요!" });
        handleChange({ name: "nicknameErr", value: "" });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handlePayment = () => {
    navigation.navigate("ShareStack", {
      screen: "Payment",
      params: {
        item: payment,
        handle: setPayment,
      },
    });
  };

  const handleSignUp = async () => {
    const data = {
      name,
      nickname,
      email,
      gender,
      phoneNumber,
      birthDay,
      username,
      provider,
    };
    try {
      const result = await signUp(data);
      const { jwt, user } = result.data;
      await storeData({ key: "token", value: jwt });
      if (payment) {
        const newPayment = { ...payment, user: user.id, name, phoneNumber };
        const { data } = await addPayment({ data: newPayment });
        user["payment"] = data;
      }
      authDispatch(setProfile({ token: jwt, profile: user }));
    } catch (e) {
      console.log(e.response.data);
    }
  };

  const confirmCode = async (code) => {
    try {
      console.log("요기");
      console.log(code);
      await confirm.confirm(code);
    } catch (error) {
      console.log("Invalid code.");
    }
  };

  return (
    <Container>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={"date"}
        onConfirm={handleConfirm}
        onCancel={() => setDatePickerVisibility(false)}
      />
      <Header navigation={navigation} center={"회원가입"}></Header>
      <View style={styles.topLineContainer}>
        <View style={styles.topLineLeft}></View>
        <View style={styles.topLineRight}></View>
      </View>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{ padding: 10 }}>
          <Text s4 bold margin={[10, 0, 10]}>
            이름<Text danger>*</Text>
          </Text>
          <TextInput
            placeholder={"이름을 입력해주세요."}
            value={name}
            onChangeText={(value) => handleChange({ name: "name", value })}
            err={nameErr}
          />
          {!!nameErr && (
            <Text s4 danger margin={[5, 5, 10]}>
              {nameErr}
            </Text>
          )}
          <Text s4 bold margin={[30, 0, 10]}>
            생년월일<Text danger>*</Text>
          </Text>
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => {
                setDatePickerVisibility(true);
              }}
            >
              <View style={styles.dateContainer}>
                <AntDesign
                  name="calendar"
                  size={22}
                  color={COLORS.gray}
                  style={styles.dateIcon}
                />
                <Text s4 g2>
                  {birthDay || "생년월일"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Text s4 bold margin={[30, 0, 10]}>
            휴대폰 번호<Text danger>*</Text>
          </Text>
          <View style={styles.row}>
            <TextInput
              placeholder={`휴대폰 번호를 입력해주세요("-"제외)`}
              value={phoneNumber}
              onChangeText={(value) =>
                handleChange({ name: "phoneNumber", value })
              }
              err={phoneErr}
              style={{ flex: 1 }}
            />
            <Button
              onPress={handlePhoneCode}
              main={phoneErr === "" && !!phoneNumber}
              textWhite={phoneErr === "" && !!phoneNumber}
              textGray={phoneErr !== "" || !phoneNumber}
              text="인증번호 요청"
              style={{
                marginLeft: 5,
                flex: 0,
              }}
            />
          </View>
          {!!phoneErr && (
            <Text s4 danger margin={[5, 5, 10]}>
              {phoneErr}
            </Text>
          )}
          {confirm && (
            <TextInput
              placeholder={"인증번호 입력하기"}
              style={{ marginTop: 10 }}
              onChangeText={(value) => confirmCode(value)}
            />
          )}
          <Text s4 bold margin={[30, 0, 10]}>
            성별<Text danger>*</Text>
          </Text>
          <View style={styles.row}>
            <Button
              lightMain={gender === "male"}
              outline={gender !== "male"}
              textGray={gender !== "male"}
              text="남자"
              onPress={() => handleChange({ name: "gender", value: "male" })}
            />
            <View style={{ width: 5 }}></View>
            <Button
              lightMain={gender === "female"}
              outline={gender !== "female"}
              textGray={gender !== "female"}
              text="여자"
              onPress={() => handleChange({ name: "gender", value: "female" })}
            />
          </View>
          <Text s4 bold margin={[30, 0, 10]}>
            이메일
          </Text>
          <TextInput
            placeholder={"이메일을 입력해주세요."}
            value={email}
            onChangeText={(value) => handleChange({ name: "email", value })}
            err={emailErr}
          />
          {!!emailErr && (
            <Text s4 danger margin={[5, 5, 10]}>
              {emailErr}
            </Text>
          )}
          <Text s4 bold margin={[30, 0, 10]}>
            닉네임<Text danger></Text>
          </Text>
          <View style={styles.row}>
            <TextInput
              placeholder={`닉네임`}
              value={nickname}
              onChangeText={(value) =>
                handleChange({ name: "nickname", value })
              }
              err={nicknameErr}
              success={nicknameSuccess}
              style={{ flex: 1 }}
            />
            <TouchableOpacity
              disabled={!nickname}
              onPress={handleCheckNick}
              style={[
                styles.nickBtn,
                nickname && {
                  borderColor: COLORS.main,
                  backgroundColor: COLORS.main,
                },
              ]}
            >
              <Text g2={!nickname} s4 white={nickname}>
                중복확인
              </Text>
            </TouchableOpacity>
          </View>
          {!!nicknameErr && (
            <Text s4 danger margin={[5, 5, 10]}>
              {nicknameErr}
            </Text>
          )}
          {!!nicknameSuccess && (
            <Text s4 success margin={[5, 5, 10]}>
              {nicknameSuccess}
            </Text>
          )}
          <Text s4 bold margin={[30, 0, 10]}>
            결제 등록
          </Text>
          {payment ? (
            <TouchableOpacity style={styles.paymentBtn} onPress={handlePayment}>
              <Text s4 bold>
                결제 정보
              </Text>
              <View style={styles.paymentText}>
                <Text
                  s3
                  margin={[0, 10, 0]}
                >{`(${payment.bankName}) ${payment.accountNumber}`}</Text>
                <AntDesign name="right" size={22} color="gray" />
              </View>
            </TouchableOpacity>
          ) : (
            <View>
              <Button gray onPress={handlePayment}>
                <View style={{ flexDirection: "row" }}>
                  <AntDesign
                    name="plus"
                    size={22}
                    color={COLORS.main}
                    style={styles.dateIcon}
                  />
                  <Text s4>결제 정보 등록하기</Text>
                </View>
              </Button>
            </View>
          )}

          <View style={{ height: 30 }}></View>
        </View>
        {Platform.OS === "ios" && <KeyboardSpacer />}
      </ScrollView>
      <Button
        main={name && phoneNumber && gender}
        gray={!name || !phoneNumber || !gender}
        text="다음 단계로"
        style={{ flex: 0, height: 60 }}
        onPress={handleSignUp}
      ></Button>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {},
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineLeft: { flex: 1, borderTopWidth: 3, borderTopColor: COLORS.main },
  topLineRight: {
    flex: 1,
    borderTopWidth: 3,
    borderTopColor: COLORS.lightGray,
  },
  dateContainer: {
    width: width * 0.4,
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: 2,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
  },
  dateIcon: { width: 22, height: 22, marginHorizontal: 10 },
  dateInput: {
    marginLeft: 30,
    borderWidth: 0,
    paddingTop: 10,
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  row: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
  },
  rightRow: {
    marginLeft: "auto",
    flexDirection: "row",
    alignItems: "center",
  },
  nickBtn: {
    borderWidth: 1,
    borderColor: COLORS.gray,
    height: 50,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  paymentBtn: {
    marginTop: 30,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  paymentText: {
    flexDirection: "row",
    alignItems: "center",
  },
});
