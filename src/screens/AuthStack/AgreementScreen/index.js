import React, { useState } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity } from "react-native";
import { Button, Text } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { AntDesign } from "@expo/vector-icons";

import COLORS from "@modules/color";

// contexts
export default ({ route, navigation }) => {
  const params = route.params;
  console.log(params);
  const [agreement1, setAgreemnet1] = useState(false);
  const [agreement2, setAgreemnet2] = useState(false);
  const [agreement3, setAgreemnet3] = useState(false);
  const [agreement4, setAgreemnet4] = useState(false);

  const [showAgreement1, setShowAgreemnet1] = useState(false);
  const [showAgreement2, setShowAgreemnet2] = useState(false);
  const [showAgreement3, setShowAgreemnet3] = useState(false);
  const [showAgreement4, setShowAgreemnet4] = useState(false);

  const handleAllAgree = () => {
    if (agreement1 && agreement2 && agreement3 && agreement4) {
      setAgreemnet1(false);
      setAgreemnet2(false);
      setAgreemnet3(false);
      setAgreemnet4(false);
    } else {
      setAgreemnet1(true);
      setAgreemnet2(true);
      setAgreemnet3(true);
      setAgreemnet4(true);
    }
  };
  const handleAgreement = async () => {
    if (agreement1 && agreement2 && agreement3 && agreement4) {
      navigation.navigate("SignUp", params);
    }
  };
  return (
    <Container>
      <Header navigation={navigation} center={"약관동의"} main></Header>
      <ScrollView style={styles.container} keyboardShouldPersistTaps="handled">
        <Agreement
          active={agreement1 && agreement2 && agreement3 && agreement4}
          text={"전체동의"}
          onPress={handleAllAgree}
        />

        <Agreement
          active={agreement1}
          text={"서비스 이용약간 동의(필수)"}
          onPress={() => setAgreemnet1(!agreement1)}
          action={() => setShowAgreemnet1(!showAgreement1)}
        />
        <AgreementText show={showAgreement1} />
        <Agreement
          active={agreement2}
          text={"개인정보 수집 및 이용 동의(필수)"}
          onPress={() => setAgreemnet2(!agreement2)}
          action={() => setShowAgreemnet2(!showAgreement2)}
        />
        <AgreementText show={showAgreement2} />
        <Agreement
          active={agreement3}
          text={"개인정보 제 3자 제공 동의(필수)"}
          onPress={() => setAgreemnet3(!agreement3)}
          action={() => setShowAgreemnet3(!showAgreement3)}
        />
        <AgreementText show={showAgreement3} />
        <Agreement
          active={agreement4}
          text={"전자 금융거래 이용약관 동의(필수)"}
          onPress={() => setAgreemnet4(!agreement4)}
          action={() => setShowAgreemnet4(!showAgreement4)}
        />
        <AgreementText show={showAgreement4} />
      </ScrollView>
      <Button
        main={agreement1 && agreement2 && agreement3 && agreement4}
        text="다음으로"
        style={{ flex: 0, height: 60 }}
        onPress={handleAgreement}
      ></Button>
    </Container>
  );
};

const Agreement = ({ active, text, onPress, action }) => {
  return (
    <View style={styles.iconContainer}>
      <TouchableOpacity
        onPress={onPress}
        style={[styles.iconBtn, active && { backgroundColor: COLORS.main }]}
      >
        <AntDesign name="check" size={18} color={"white"} />
      </TouchableOpacity>
      <Text s4 margin={[0, 0, 0, 10]}>
        {text}
      </Text>
      {action && (
        <TouchableOpacity style={styles.modeBtn} onPress={action}>
          <AntDesign name="right" size={18} color={COLORS.gray} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const AgreementText = ({ show }) => {
  return (
    <View
      style={{
        ...styles.textContainer,
        display: show ? "flex" : "none",
      }}
    >
      <Text s4 g1>
        * 하다 서비스 이용약관 {"\n\n"} 해당 서비스는 일거리 매칭 의뢰할 수 있는
        플랫폼입니다. 서비스를 불법적으로 악용하는 사례가 발생할 경우 '하다'
        측에서 금전적 보상을 요구할 수 있습니다.
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  iconContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.playGray,
  },
  iconBtn: { padding: 5, borderRadius: 50, backgroundColor: COLORS.gray },
  modeBtn: {
    padding: 5,
    borderRadius: 50,
    marginLeft: "auto",
  },
  textContainer: {
    padding: 20,
    backgroundColor: COLORS.playGray,
  },
});
