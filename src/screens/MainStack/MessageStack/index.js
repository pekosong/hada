import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import MessageScreen from "./MessageScreen";

const MessageStack = createStackNavigator();
function MessageStackNavigation() {
  return (
    <MessageStack.Navigator headerMode="none">
      <MessageStack.Screen
        name="Message"
        component={MessageScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </MessageStack.Navigator>
  );
}

export default MessageStackNavigation;
