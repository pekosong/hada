import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { Text, Divider } from "@components";
import { useSelector } from "react-redux";
import { selectMessage } from "@stores/message";
import { selectAuth } from "@stores/auth";
import { getUsersByIds } from "@services/auth";
import { fromNow } from "@modules/util";

const avatar = require("@assets/images/avatar.png");

// contexts
export default ({ navigation }) => {
  const { messages } = useSelector(selectMessage);
  const { profile } = useSelector(selectAuth);

  return (
    <Container top>
      <Header border center={"메시지"}></Header>
      {profile && (
        <FlatList
          style={styles.container}
          ItemSeparatorComponent={() => (
            <Divider width={1} padding={0}></Divider>
          )}
          data={messages}
          renderItem={({ item, index }) => {
            return (
              <ChatCard
                first={index === 0}
                item={item}
                userId={profile.id}
                navigation={navigation}
              />
            );
          }}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </Container>
  );
};

const ChatCard = ({ navigation, item, first, userId }) => {
  const [profile, setProfile] = useState(null);
  const user = userId.toString() !== item.host ? item.host : item.user;
  useEffect(() => {
    getUsersByIds("id=" + user)
      .then(({ data }) => {
        setProfile(data[0]);
      })
      .catch((err) => console.log(err.response.data));
  }, []);
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate("ShareStack", {
          screen: "MessageDetail",
          params: { doc: item.id, postId: item.postId, user },
        });
      }}
    >
      <View style={[styles.cardContainer, first && { marginTop: 10 }]}>
        <Image
          source={
            profile && profile.profile
              ? { uri: profile.profile["url"] }
              : avatar
          }
          style={styles.image}
        />
        <View>
          {profile && (
            <Text s5 bold margin={[0, 0, 5]}>
              {profile.nickname ? profile.nickname : profile.name}
              <Text g2 s6>
                {"  -  " + fromNow(item.createdAt)}
              </Text>
            </Text>
          )}
          <Text s5>{item.text}</Text>
        </View>
        {item.unRead !== 0 && (
          <View style={styles.tag}>
            <Text s5 white>
              {item.unRead}
            </Text>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {},
  cardContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: "row",
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 40,
    marginRight: 10,
  },
  tag: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "auto",
    width: 25,
    height: 25,
    borderRadius: 25,
    backgroundColor: "rgb(255, 101, 101)",
  },
});
