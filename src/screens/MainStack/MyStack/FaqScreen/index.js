import React, { useState } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity } from "react-native";
import { Text } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { AntDesign } from "@expo/vector-icons";

import COLORS from "@modules/color";

// contexts
export default ({ navigation }) => {
  const [showAgreement1, setShowAgreemnet1] = useState(false);
  const [showAgreement2, setShowAgreemnet2] = useState(false);
  const [showAgreement3, setShowAgreemnet3] = useState(false);

  return (
    <Container>
      <Header navigation={navigation} center={"FAQ"}></Header>
      <ScrollView style={styles.container} keyboardShouldPersistTaps="handled">
        <Faq
          text={"Q. 미션등록할때 몇개까지 등록이 가능한가요?"}
          action={() => setShowAgreemnet1(!showAgreement1)}
        />
        <FaqText show={showAgreement1} />
        <Faq
          text={"Q. 재능등록시 거짓정보나 정확하지않은 정보를 기입할때 …"}
          action={() => setShowAgreemnet2(!showAgreement2)}
        />
        <FaqText show={showAgreement2} />
        <Faq
          text={"Q. 미션등록할때 몇개까지 등록이 가능한가요?"}
          action={() => setShowAgreemnet3(!showAgreement3)}
        />
        <FaqText show={showAgreement3} />
      </ScrollView>
    </Container>
  );
};

const Faq = ({ text, action }) => {
  return (
    <View style={styles.iconContainer}>
      <View style={{ flex: 1 }}>
        <Text s5 margin={[0, 0, 0, 10]}>
          {text}
        </Text>
      </View>
      {action && (
        <TouchableOpacity style={styles.modeBtn} onPress={action}>
          <AntDesign name="down" size={16} color={COLORS.gray} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const FaqText = ({ show }) => {
  return (
    <View
      style={{
        ...styles.textContainer,
        display: show ? "flex" : "none",
      }}
    >
      <Text s5 g1>
        미션 등록시, 하루 최대 10개까지 가능합니다.{"\n\n"}
        하지만 무분별한 등록은 권장하지 않습니다.{"\n\n"}
        필요한 미션만 등록해주세요~
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.playGray,
    paddingTop: 10,
  },
  iconContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.playGray,
    backgroundColor: COLORS.white,
  },
  iconBtn: { padding: 5, borderRadius: 50, backgroundColor: COLORS.gray },
  modeBtn: {
    padding: 5,
    borderRadius: 50,
    marginLeft: "auto",
  },
  textContainer: {
    padding: 20,
    backgroundColor: COLORS.white,
  },
});
