import React from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { Text, MainCard, Divider } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import COLORS from "@modules/color";
import { getPostById } from "@services/post";

import { useSelector } from "react-redux";
import { selectLike } from "@stores/like";

const FirstTab = ({ navigation }) => {
  const { likes } = useSelector(selectLike);
  const missions = likes
    .filter((el) => el.type === "mission")
    .sort((a, b) => b.create_at > a.create_at);

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text>{`총 ${missions.length}개`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={missions}
        renderItem={({ item }) => (
          <MainCard item={item} onPress={() => handleClick(item)}></MainCard>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const SecondTab = ({ navigation }) => {
  const { likes } = useSelector(selectLike);
  const talents = likes
    .filter((el) => el.type === "talent")
    .sort((a, b) => b.create_at > a.create_at);

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text>{`총 ${talents.length}개`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={talents}
        renderItem={({ item }) => (
          <MainCard item={item} onPress={() => handleClick(item)}></MainCard>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ navigation }) => {
  return (
    <Container top style={{ flex: 1 }}>
      <Header navigation={navigation}></Header>
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen name="관심 미션" component={FirstTab} />
        <Tab.Screen name="관심 재능" component={SecondTab} />
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
  },
});
