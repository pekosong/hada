import React, { useState } from "react";
import { StyleSheet, View, Image, ScrollView, Linking } from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { Text, PostModal } from "@components";
import { AirbnbRating } from "react-native-ratings";
import { AntDesign } from "@expo/vector-icons";

import { TouchableOpacity } from "react-native-gesture-handler";
import COLORS from "@modules/color";
import { removeData, getSize, calcRating } from "@modules/util";

import { useSelector, useDispatch } from "react-redux";
import { setLogout, selectAuth } from "@stores/auth";
import { selectPost } from "@stores/post";

const { width } = getSize();

const bell = require("@assets/icons/bell.png");
const setting = require("@assets/icons/setting.png");

const review = require("@assets/icons/review.png");
const mission = require("@assets/icons/mission.png");
const talent = require("@assets/icons/talent.png");

const avatar = require("@assets/images/avatar.png");

// contexts
export default ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  const [title, setTitle] = useState("");
  const [menuItems, setMenuItems] = useState([]);
  const auth = useSelector(selectAuth);
  const post = useSelector(selectPost);

  const dispatch = useDispatch();

  const handleLogout = async () => {
    await removeData("token");
    dispatch(setLogout());
  };
  if (!auth.profile) return null;

  const handleWarn = () => {
    setTitle("신고 하시겠습니까?");
    setMenuItems([
      {
        text: "신고하기",
        onPress: () => {
          navigation.navigate("ShareStack", {
            screen: "Report",
          });
        },
      },
      {
        text: "취소",
        onPress: () => {},
      },
    ]);
    setShowModal(true);
  };

  const handleCustomer = () => {
    setTitle("고객센터");
    setMenuItems([
      {
        text: "문의를 위해 메일 보내기",
        onPress: () => {
          Linking.openURL("mailto:funnybrown@naver.com");
        },
      },
      {
        text: "취소",
        onPress: () => {},
      },
    ]);
    setShowModal(true);
  };
  return (
    <Container top>
      <PostModal
        showModal={showModal}
        setShowModal={setShowModal}
        title={title}
        menuItems={menuItems}
      />
      <Header
        border
        center="마이페이지"
        right={
          <View style={styles.headerContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("ShareStack", {
                  screen: "Notification",
                })
              }
            >
              <Image style={styles.searchIcon} source={bell} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate("ProfileEdit")}
            >
              <Image style={styles.notificationIcon} source={setting} />
            </TouchableOpacity>
          </View>
        }
      ></Header>
      <ScrollView style={{ backgroundColor: COLORS.playGray }}>
        <View style={{ backgroundColor: COLORS.white }}>
          <View style={styles.profileContainer}>
            <Image
              source={
                auth.profile.profile
                  ? { uri: auth.profile.profile["url"] }
                  : avatar
              }
              style={styles.avatar}
            />
            <View style={{ marginLeft: 10 }}>
              <Text bold s3>
                {!!auth.profile.nickname
                  ? auth.profile.nickname
                  : auth.profile.name}
              </Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <AirbnbRating
                  showRating={false}
                  count={5}
                  defaultRating={calcRating(auth.profile.reviews)}
                  size={13}
                />
                <Text s4 margin={[0, 0, 0, 5]}>
                  {calcRating(auth.profile.reviews)}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryContainer}>
            <SummaryItem
              image={mission}
              text={"등록한 미션"}
              count={post.posts.filter((el) => el.type === "mission").length}
              onPress={() => navigation.navigate("Post", { type: "mission" })}
            />
            <View style={styles.verticalDivider}></View>
            <SummaryItem
              image={talent}
              text={"등록한 재능"}
              count={post.posts.filter((el) => el.type === "talent").length}
              onPress={() => navigation.navigate("Post", { type: "talent" })}
            />
            <View style={styles.verticalDivider}></View>
            <SummaryItem
              image={review}
              text={"후기보기"}
              count={auth.profile.reviews.length}
              onPress={() =>
                navigation.navigate("ShareStack", {
                  screen: "Review",
                  params: {},
                })
              }
            />
          </View>
        </View>
        <View style={{ height: 10 }}></View>
        <View>
          <MenuItem
            text="관심 목록"
            onPress={() => navigation.navigate("Like")}
          />
          <MenuItem
            text="기본정보"
            onPress={() => navigation.navigate("Info")}
          />
          <MenuItem text="신고" onPress={handleWarn} />
          <MenuItem text="FAQ" onPress={() => navigation.navigate("Faq")} />
          <MenuItem text="고객센터" onPress={handleCustomer} />
          <MenuItem text="로그아웃" onPress={handleLogout} />
        </View>
      </ScrollView>
    </Container>
  );
};

const SummaryItem = ({ image, text, count, onPress }) => {
  return (
    <TouchableOpacity style={styles.summaryItem} onPress={onPress}>
      <Image source={image} style={{ width: 25, height: 25 }}></Image>
      <Text s5 g1 margin={[5, 0, 5]}>
        {text}
      </Text>
      <Text bold>{count}</Text>
    </TouchableOpacity>
  );
};

const MenuItem = ({ text, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.menuItem}>
        <Text margin={[5, 0, 0]}>{text}</Text>
        <AntDesign name="right" size={22} color="gray" />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
  },
  searchIcon: {
    width: 22,
    height: 22,
    marginLeft: "auto",
    marginRight: 10,
  },
  notificationIcon: {
    width: 22,
    height: 22,
  },
  profileContainer: {
    marginTop: 20,
    height: 70,
    margin: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
  summaryContainer: {
    flex: 1,
    backgroundColor: COLORS.playGray,
    height: 120,
    marginHorizontal: 10,
    marginBottom: 10,
    borderRadius: 10,
    alignItems: "center",
    flexDirection: "row",
  },
  summaryItem: {
    width: width / 3.2,
    alignItems: "center",
  },
  verticalDivider: {
    borderRightWidth: 1,
    borderRightColor: COLORS.lightGray,
    height: 60,
  },
  menuItem: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: COLORS.white,
  },
});
