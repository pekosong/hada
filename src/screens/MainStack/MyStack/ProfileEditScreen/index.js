import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { AntDesign } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import COLORS from "@modules/color";

import { useSelector, useDispatch } from "react-redux";
import { updateProfileAsync, selectAuth } from "@stores/auth";
import { uploadImage } from "@services/image";

const avatar = require("@assets/images/avatar.png");

// contexts
export default ({ navigation }) => {
  const dispatch = useDispatch();
  const auth = useSelector(selectAuth);

  const [nickname, setNickname] = useState(auth.profile.nickname ?? "");
  const [image, setImage] = useState(null);
  const [preview, setPreview] = useState(null);

  useEffect(() => {
    getPermissionAsync();
  }, []);
  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  const _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
    });
    if (!result.cancelled) {
      const uri = result.uri;
      let uriParts = uri.split(".");
      let fileType = uriParts[uriParts.length - 1];
      let formData = new FormData();
      formData.append("files", {
        uri,
        name: `file.${fileType}`,
        type: `image/${fileType}`,
      });
      setImage(formData);
      setPreview(result.uri);
    }
  };

  const handleConfirm = async () => {
    const id = await uploadImage(image);
    const data = {
      id: auth.profile.id,
      nickname: nickname,
      profile: id,
    };
    dispatch(updateProfileAsync({ profile: data }));
    navigation.goBack();
  };
  if (!auth.profile) return null;
  return (
    <Container>
      <Header
        navigation={navigation}
        center={"프로필"}
        right={
          <TouchableOpacity onPress={handleConfirm} style={{ padding: 10 }}>
            <AntDesign name="check" size={22} color={COLORS.main} />
          </TouchableOpacity>
        }
      />
      <View style={styles.topLineContainer}>
        <View style={styles.topLineRight}></View>
      </View>
      <KeyboardAwareScrollView
        style={styles.container}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <TouchableOpacity style={styles.cameraContainer} onPress={_pickImage}>
          <Image
            source={
              preview
                ? { uri: preview }
                : auth.profile.profile
                ? { uri: auth.profile.profile["url"] }
                : avatar
            }
            style={styles.image}
          />
        </TouchableOpacity>
        <Text center s4 main bold margin={[0, 0, 10]}>
          프로필 사진 변경
        </Text>
        <Text s4 bold margin={[0, 0, 10]}>
          닉네임
        </Text>
        <TextInput
          placeholder={"미입력시 이름으로 활동합니다."}
          value={nickname}
          onChangeText={(value) => {
            setNickname(value);
          }}
        />
      </KeyboardAwareScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineRight: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: COLORS.lightGray,
  },
  cameraContainer: {
    alignItems: "center",
    paddingVertical: 10,
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 70,
  },
});
