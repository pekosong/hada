import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import MyScreen from "./MyScreen";
import LikeScreen from "./LikeScreen";
import ProfileEditScreen from "./ProfileEditScreen";
import InfoScreen from "./InfoScreen";
import PostScreen from "./PostScreen";
import ApplicantScreen from "./ApplicantScreen";
import FaqScreen from "./FaqScreen";

const MyStack = createStackNavigator();
function MyStackNavigation() {
  return (
    <MyStack.Navigator headerMode="none">
      <MyStack.Screen
        name="My"
        component={MyScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="Like"
        component={LikeScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="ProfileEdit"
        component={ProfileEditScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="Info"
        component={InfoScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="Post"
        component={PostScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="Applicant"
        component={ApplicantScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <MyStack.Screen
        name="Faq"
        component={FaqScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </MyStack.Navigator>
  );
}

export default MyStackNavigation;
