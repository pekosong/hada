import React, { useState } from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { Text, Divider, MainCard, PostModal } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import COLORS from "@modules/color";

import { useSelector, useDispatch } from "react-redux";
import { selectPost, removePost, modifyPost } from "@stores/post";
import { getPostById, updatePost, removePostById } from "@services/post";

const FirstTab = ({ typeName, navigation, reviews }) => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [item, setItem] = useState(null);

  const menuItems = [
    {
      text: typeName + " 수정",
      onPress: () => {
        navigation.navigate("ShareStack", {
          screen: "Add",
          params: { item },
        });
      },
    },
    {
      text: typeName + " 삭제",
      onPress: async () => {
        try {
          await removePostById(item.id);
          dispatch(removePost(item.id));
        } catch (err) {
          console.log(err);
        }
      },
    },
    {
      text: "참가자 관리",
      onPress: async () => {
        try {
          const { data } = await getPostById(item.id);
          navigation.navigate("Applicant", { item: data });
        } catch (err) {
          console.log(err);
        }
      },
    },
  ];

  const HandleUpdatePost = async ({ item, status }) => {
    const result = await updatePost({ id: item.id, data: { status } });
    dispatch(modifyPost(result.data));
  };

  return (
    <View style={{ flex: 1 }}>
      <PostModal
        showModal={showModal}
        setShowModal={setShowModal}
        title={typeName + " 관리(모집중)"}
        menuItems={menuItems}
      />
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text margin={[0, 0, 0, 5]}>{`총 ${reviews.length}개`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => (
          <MainCard
            item={item}
            onMenuPress={() => {
              setShowModal(true);
              setItem(item);
            }}
            HandleUpdatePostProcess={() =>
              HandleUpdatePost({ item, status: "proceeding" })
            }
            HandleUpdateCompleteProcess={() => {
              HandleUpdatePost({ item, status: "finished" });
            }}
            menu
          ></MainCard>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const SecondTab = ({ typeName, navigation, reviews }) => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [item, setItem] = useState(null);

  const menuItems = [
    {
      text: typeName + " 삭제",
      onPress: async () => {
        try {
          await removePostById(item.id);
          dispatch(removePost(item.id));
        } catch (err) {
          console.log(err);
        }
      },
    },
    {
      text: "참가자 관리",
      onPress: async () => {
        try {
          const { data } = await getPostById(item.id);
          navigation.navigate("Applicant", { item: data });
        } catch (err) {
          console.log(err);
        }
      },
    },
  ];
  const HandleUpdatePost = async ({ item, status }) => {
    const result = await updatePost({ id: item.id, data: { status } });
    dispatch(modifyPost(result.data));
  };

  return (
    <View style={{ flex: 1 }}>
      <PostModal
        showModal={showModal}
        setShowModal={setShowModal}
        title={typeName + " 관리(모집중)"}
        menuItems={menuItems}
      />
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text margin={[0, 0, 0, 5]}>{`총 ${reviews.length}개`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => (
          <MainCard
            item={item}
            onMenuPress={() => {
              setShowModal(true);
              setItem(item);
            }}
            HandleUpdateCompleteProcess={() => {
              HandleUpdatePost({ item, status: "finished" });
            }}
            menu
          ></MainCard>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const ThirdTab = ({ typeName, navigation, reviews }) => {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [item, setItem] = useState(null);

  const menuItems = [
    {
      text: typeName + " 삭제",
      onPress: async () => {
        try {
          await removePostById(item.id);
          dispatch(removePost(item.id));
        } catch (err) {
          console.log(err);
        }
      },
    },
    {
      text: "참가자 관리",
      onPress: async () => {
        try {
          const { data } = await getPostById(item.id);
          navigation.navigate("Applicant", { item: data });
        } catch (err) {
          console.log(err);
        }
      },
    },
  ];

  return (
    <View style={{ flex: 1 }}>
      <PostModal
        showModal={showModal}
        setShowModal={setShowModal}
        title={typeName + " 관리(모집중)"}
        menuItems={menuItems}
      />
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text margin={[0, 0, 0, 5]}>{`총 ${reviews.length}개`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => (
          <MainCard
            item={item}
            onMenuPress={() => {
              setShowModal(true);
              setItem(item);
            }}
            menu
          ></MainCard>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ route, navigation }) => {
  const { type } = route.params;
  const post = useSelector(selectPost);
  const data = post.posts
    .filter((el) => el.type === type)
    .sort((a, b) => b.created_at > a.created_at);

  const recruiting = data.filter((el) => el.status === "recruiting");
  const finished = data.filter((el) => el.status === "finished");
  const process = data.filter((el) => el.status === "proceeding");

  const typeName = type === "mission" ? "미션" : "재능";
  return (
    <Container top style={{ flex: 1 }}>
      <Header
        navigation={navigation}
        center={`내가 등록한 ${typeName}`}
      ></Header>
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen name="모집 중">
          {(props) => (
            <FirstTab {...props} typeName={typeName} reviews={recruiting} />
          )}
        </Tab.Screen>
        <Tab.Screen name="수행 중">
          {(props) => (
            <SecondTab {...props} typeName={typeName} reviews={process} />
          )}
        </Tab.Screen>
        <Tab.Screen name="완료된 미션">
          {(props) => (
            <ThirdTab {...props} typeName={typeName} reviews={finished} />
          )}
        </Tab.Screen>
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
    alignItems: "center",
  },
});
