import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import { AirbnbRating } from "react-native-ratings";

import { Text, Divider, Modal } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { updateApplication, getPostApplications } from "@services/application";

import { fromNow } from "@modules/util";
import COLORS from "@modules/color";
import { Entypo } from "@expo/vector-icons";

const avatar = require("@assets/images/avatar.png");

// contexts
export default ({ route, navigation }) => {
  const { item } = route.params;
  const [showModal, setShowModal] = useState(false);
  const [application, setApplication] = useState("");
  const [applications, setApplications] = useState([]);

  useEffect(() => {
    setApplicationsData();
  }, []);

  const setApplicationsData = () => {
    getPostApplications(item.id)
      .then((data) => {
        setApplications(data.filter((el) => el.user));
      })
      .catch((err) => console.log(err.response.data));
  };
  const handleChange = async (status) => {
    await updateApplication({ id: application, status });
  };

  return (
    <Container top style={{ flex: 1 }}>
      <Modal showModal={showModal} setShowModal={setShowModal}>
        <Text s2 bold margin={[0, 0, 20]}>
          참여자 선택
        </Text>
        <TouchableOpacity
          onPress={() => {
            setShowModal(false);
            handleChange("success");
            setApplicationsData();
          }}
        >
          <View style={styles.modalItem}>
            <Text>수락</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setShowModal(false);
            handleChange("deny");
            setApplicationsData();
          }}
        >
          <View style={styles.modalItem}>
            <Text>거절</Text>
          </View>
        </TouchableOpacity>
      </Modal>
      <Header
        navigation={navigation}
        center={`참여자 관리(${
          item.status === "apply"
            ? "모집 중"
            : item.status === "proceeding"
            ? "수행중"
            : "완료"
        })`}
      ></Header>
      <View style={{ flex: 1 }}>
        {applications.length === 0 ? (
          <View style={{ flex: 1, justifyContent: "center" }}>
            <Text g1 s4 center>
              지원자가 없습니다.
            </Text>
          </View>
        ) : (
          <FlatList
            ItemSeparatorComponent={() => <Divider width={1}></Divider>}
            data={applications}
            renderItem={({ item }) => (
              <ApplicantCard
                data={item}
                onPressModal={() => {
                  setShowModal(true);
                  setApplication(item["id"]);
                }}
                onPress={() =>
                  navigation.navigate("ShareStack", {
                    screen: "Profile",
                    params: { user: item.user },
                  })
                }
              ></ApplicantCard>
            )}
            keyExtractor={(item) => item.id.toString()}
          />
        )}
      </View>
    </Container>
  );
};

const ApplicantCard = ({ data, onPress, onPressModal }) => {
  const { user } = data;
  return (
    <View style={styles.cardContainer}>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image
          source={user.profile ? { uri: user.profile["url"] } : avatar}
          style={styles.avatar}
        />

        <View>
          <Text s4>
            {(user.nickname || user.name) + " - "}
            <Text s5 g1>
              {fromNow(data.created_at)}
            </Text>
          </Text>
          <View style={styles.ratingContainer}>
            <AirbnbRating
              showRating={false}
              count={5}
              defaultRating={4}
              size={12}
            />
            <Text s5 margin={[0, 0, 0, 5]}>
              3.5
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      {data.status === "apply" ? (
        <TouchableOpacity
          onPress={onPressModal}
          style={{ marginLeft: "auto", padding: 10 }}
        >
          <View>
            <Entypo name={"dots-three-vertical"} size={14} />
          </View>
        </TouchableOpacity>
      ) : (
        <Text style={{ marginLeft: "auto", padding: 10 }}>
          {data.status === "deny" ? "거절" : "수락"}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  cardContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  container: { flexDirection: "row", alignItems: "center" },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 30,
    marginRight: 10,
  },
  ratingContainer: { flexDirection: "row", alignItems: "center" },
  tag: {
    backgroundColor: COLORS.lightGray,
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  modalItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
});
