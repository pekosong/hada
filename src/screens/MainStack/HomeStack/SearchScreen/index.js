import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Keyboard,
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { MainCard, Divider, Loader, Text } from "@components";

import Container from "@layouts/container";
import COLORS from "@modules/color";
import { getData, storeData, removeData } from "@modules/util";

import { getPostById } from "@services/post";
import useRequestSearch from "@hooks/useRequestSearch";
import { AntDesign } from "@expo/vector-icons";

const PostTab = ({ navigation, type, data }) => {
  const { posts, loading, loadMoreData } = data;

  if (posts.length === 0)
    return (
      <View style={styles.noResult}>
        <Text>검색 결과가 없습니다.</Text>
      </View>
    );

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  let endReachCall;

  const handleOnEndReached = () => {
    if (!endReachCall) {
      endReachCall = setTimeout(() => {
        loadMoreData();
        endReachCall = false;
      }, 1000);
    }
  };

  const ListFooterComponent = () =>
    loading && (
      <View style={{ marginVertical: 10 }}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    );

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={posts}
        renderItem={({ item, index }) => {
          return (
            <>
              <View style={{ marginTop: index === 0 ? 20 : 0 }}></View>
              <MainCard
                item={item}
                onPress={() => handleClick(item)}
              ></MainCard>
            </>
          );
        }}
        keyExtractor={(item) => type + item.id.toString()}
        onEndReached={handleOnEndReached}
        onEndReachedThreshold={0.4}
        ListFooterComponent={ListFooterComponent}
      />
    </View>
  );
};

const MissionTab = ({ navigation, type, data }) => {
  const { posts, loading, loadMoreData } = data;

  if (posts.length === 0)
    return (
      <View style={styles.noResult}>
        <Text>검색 결과가 없습니다.</Text>
      </View>
    );

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  let endReachCall;

  const handleOnEndReached = () => {
    if (!endReachCall) {
      endReachCall = setTimeout(() => {
        loadMoreData();
        endReachCall = false;
      }, 1000);
    }
  };

  const ListFooterComponent = () =>
    loading && (
      <View style={{ marginVertical: 10 }}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    );

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={posts}
        renderItem={({ item, index }) => {
          return (
            <>
              <View style={{ marginTop: index === 0 ? 20 : 0 }}></View>
              <MainCard
                item={item}
                onPress={() => handleClick(item)}
              ></MainCard>
            </>
          );
        }}
        keyExtractor={(item) => type + item.id.toString()}
        onEndReached={handleOnEndReached}
        onEndReachedThreshold={0.4}
        ListFooterComponent={ListFooterComponent}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ navigation }) => {
  const [isShow, setShow] = useState(true);
  const [text, setText] = useState("");
  const mission = useRequestSearch("mission");
  const talent = useRequestSearch("talent");

  const handleSubmit = async (e) => {
    mission.setKeyword(text);
    talent.setKeyword(text);
    const recents = await getData("recent");
    if (recents) {
      recents.push(text);
      storeData({ key: "recent", value: recents });
    } else {
      storeData({ key: "recent", value: [text] });
    }
    setShow(false);
    Keyboard.dismiss();
  };

  const handlePressRecent = (value) => {
    mission.setKeyword(value);
    talent.setKeyword(value);
    setShow(false);
    setText(value);
  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => setShow(true));
    Keyboard.addListener("keyboardDidHide", () => {});

    // cleanup function
    return () => {
      Keyboard.removeListener("keyboardDidShow", () => setShow(true));
      Keyboard.removeListener("keyboardDidHide", () => {});
    };
  }, []);

  return (
    <Container top style={{ flex: 1 }}>
      <View style={[styles.container]}>
        <View style={styles.left}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ padding: 5 }}
          >
            <AntDesign name="left" size={20} color={"#333"} />
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <AntDesign
            name="search1"
            size={20}
            color={"#333"}
            style={styles.searchIcon}
          />
          <TextInput
            value={text}
            onChangeText={(text) => setText(text.trim())}
            placeholder="내 주변 미션을 검색해봐요"
            style={styles.input}
            returnKeyType="search"
            autoFocus={true}
            clearButtonMode="while-editing"
            onSubmitEditing={handleSubmit}
          ></TextInput>
        </View>
      </View>
      {isShow && <RecentSearch handlePress={handlePressRecent} />}
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen
          name="내 주변 미션"
          navigation={navigation}
          children={(props) => (
            <PostTab type={"mission"} data={mission} {...props} />
          )}
        />
        <Tab.Screen
          name="내 주변 재능"
          navigation={navigation}
          children={(props) => (
            <MissionTab type={"talent"} data={talent} {...props} />
          )}
        />
      </Tab.Navigator>
    </Container>
  );
};

const RecentSearch = ({ handlePress }) => {
  const [recents, setRecents] = useState([]);
  useEffect(() => {
    getData("recent").then((value) => {
      if (value) setRecents(value.reverse());
    });
  }, []);

  const handleRemoveAll = () => {
    removeData("recent");
    setRecents([]);
  };
  return (
    <View style={styles.recentContainer}>
      <View style={styles.recentHeader}>
        <Text s4>최근 검색어</Text>
        <TouchableOpacity onPress={handleRemoveAll}>
          <Text s4>전체 삭제</Text>
        </TouchableOpacity>
      </View>
      {recents.length === 0 ? (
        <Text center margin={[20, 0]}>
          최근 검색 결과가 없습니다.
        </Text>
      ) : (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={{ paddingVertical: 10 }}
          ItemSeparatorComponent={() => <Divider width={1}></Divider>}
          data={recents}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                style={{ flex: 1, paddingVertical: 5 }}
                onPress={() => handlePress(item)}
              >
                <Text>{item}</Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  noResult: { flex: 1, justifyContent: "center", alignItems: "center" },
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
  },
  container: {
    display: "flex",
    flexDirection: "row",
    height: 50,
    paddingHorizontal: 10,
    justifyContent: "space-between",
    alignItems: "center",
  },
  searchIcon: {
    position: "absolute",
    left: 10,
    zIndex: 100,
    top: 10,
  },
  input: {
    backgroundColor: COLORS.playGray,
    height: 40,
    borderRadius: 5,
    paddingLeft: 40,
  },
  recentContainer: {
    position: "absolute",
    width: "100%",
    top: 40,
    zIndex: 100,
    backgroundColor: "#fff",
    paddingHorizontal: 15,
    maxHeight: 350,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.lightGray,
  },
  recentHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
});
