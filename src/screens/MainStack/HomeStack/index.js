import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import HomeScreen from "./HomeScreen";
import SearchScreen from "./SearchScreen";

const HomeStack = createStackNavigator();
function HomeStackNavigation() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <HomeStack.Screen
        name="Search"
        component={SearchScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </HomeStack.Navigator>
  );
}

export default HomeStackNavigation;
