import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import {
  Text,
  MainCard,
  Divider,
  MainModal,
  FilterIcon,
  Loader,
  ModalWebView,
} from "@components";

import Container from "@layouts/container";
import Header from "@layouts/header";
import COLORS from "@modules/color";
import { AntDesign, Feather } from "@expo/vector-icons";
import { getPostById } from "@services/post";
import { storeData, getData } from "@modules/util";

import useRequestPost from "@hooks/useRequestPost";
import useLocation from "@hooks/useLocation";
import useMap from "@hooks/useMap";

const bell = require("@assets/icons/bell.png");
const search = require("@assets/icons/search.png");

const PostTab = ({ navigation, type, data }) => {
  const [showModal, setShowModal] = useState(false);
  const [modalType, setModalType] = useState("");
  const {
    posts,
    loading,
    loadMoreData,
    sortType,
    setSortType,
    filterType,
    setFilterType,
  } = data;

  if (posts.length === 0) return <Loader />;

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  let endReachCall;

  const handleOnEndReached = () => {
    if (!endReachCall) {
      endReachCall = setTimeout(() => {
        loadMoreData();
        endReachCall = false;
      }, 1000);
    }
  };

  const ListHeaderComponent = () => (
    <View style={styles.listHeader}>
      {[
        { type: "status", text: "전체" },
        { type: "price", text: "금액" },
        { type: "day", text: "등록순" },
        { type: "priceType", text: "임금" },
      ].map((el) => (
        <FilterIcon
          key={el.type}
          type={el.type}
          sortType={sortType}
          filterType={filterType}
          text={el.text}
          onPress={() => {
            setModalType(el.type);
            setShowModal(true);
          }}
        />
      ))}
    </View>
  );

  const ListFooterComponent = () =>
    loading && (
      <View style={{ marginVertical: 10 }}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    );

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={ListHeaderComponent}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={posts}
        renderItem={({ item }) => (
          <MainCard item={item} onPress={() => handleClick(item)}></MainCard>
        )}
        keyExtractor={(item) => type + item.id.toString()}
        onEndReached={handleOnEndReached}
        onEndReachedThreshold={0.4}
        ListFooterComponent={ListFooterComponent}
      />
      <MainModal
        showModal={showModal}
        setShowModal={setShowModal}
        modalType={modalType}
        sortType={sortType}
        setSortType={setSortType}
        filterType={filterType}
        setFilterType={setFilterType}
      />
    </View>
  );
};

const MissionTab = ({ navigation, type, data }) => {
  const [showModal, setShowModal] = useState(false);
  const [modalType, setModalType] = useState("");
  const {
    posts,
    loading,
    loadMoreData,
    sortType,
    setSortType,
    filterType,
    setFilterType,
  } = data;
  if (posts.length === 0) return <Loader />;

  const handleClick = async (item) => {
    const { data } = await getPostById(item.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  let endReachCall;

  const handleOnEndReached = () => {
    if (!endReachCall) {
      endReachCall = setTimeout(() => {
        loadMoreData();
        endReachCall = false;
      }, 1000);
    }
  };

  const ListHeaderComponent = () => (
    <View style={styles.listHeader}>
      {[
        { type: "status", text: "전체" },
        { type: "price", text: "금액" },
        { type: "day", text: "등록순" },
      ].map((el) => (
        <FilterIcon
          key={el.type}
          type={el.type}
          sortType={sortType}
          filterType={filterType}
          setFilterType={setFilterType}
          text={el.text}
          onPress={() => {
            setModalType(el.type);
            setShowModal(true);
          }}
        />
      ))}
    </View>
  );

  const ListFooterComponent = () =>
    loading && (
      <View style={{ marginVertical: 10 }}>
        <ActivityIndicator></ActivityIndicator>
      </View>
    );

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        ListHeaderComponent={ListHeaderComponent}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={posts}
        renderItem={({ item }) => (
          <MainCard item={item} onPress={() => handleClick(item)}></MainCard>
        )}
        keyExtractor={(item) => type + item.id.toString()}
        onEndReached={handleOnEndReached}
        onEndReachedThreshold={0.4}
        ListFooterComponent={ListFooterComponent}
      />
      <MainModal
        showModal={showModal}
        setShowModal={setShowModal}
        modalType={modalType}
        sortType={sortType}
        setSortType={setSortType}
        filterType={filterType}
        setFilterType={setFilterType}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  const mission = useRequestPost("mission");
  const talent = useRequestPost("talent");

  useEffect(() => {
    getData("address").then((value) => {
      if (!value) {
        setShowModal(true);
      }
    });
  }, []);
  const {
    location,
    address,
    isShow,
    setLocation,
    setAddress,
    setShow,
  } = useMap();

  const reloadData = () => {
    mission.fetchData();
    talent.fetchData();
  };

  useLocation(reloadData);

  return (
    <Container top style={{ flex: 1 }}>
      {showModal && (
        <View style={styles.setLocaiton}>
          <TouchableOpacity
            style={{
              width: "80%",
              backgroundColor: COLORS.main,
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: 15,
            }}
            onPress={() => setShow(true)}
          >
            <Text white>주소 등록하기</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: "20%",
              backgroundColor: "#eee",
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: 17,
            }}
            onPress={() => setShowModal(false)}
          >
            <AntDesign name="close" size={20} color="#888" />
          </TouchableOpacity>
        </View>
      )}

      <ModalWebView
        showModal={isShow}
        setShowModal={setShow}
        lat={location?.latitude}
        lng={location?.longitude}
        onMessage={async (msg) => {
          const [address, latitude, longitude] = msg.nativeEvent.data.split(
            ","
          );
          if (address) {
            setAddress(address);
            setLocation({ latitude, longitude });
            await storeData({
              key: "location",
              value: { latitude, longitude },
            });
            await storeData({ key: "address", value: address });
            reloadData();
          }
          setShow(false);
        }}
      />
      {!showModal && (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("ShareStack", {
              screen: "Add",
              params: { reloadData },
            });
          }}
          style={styles.uploadButton}
        >
          <Feather name="edit-2" size={25} color="white" />
        </TouchableOpacity>
      )}

      <Header
        left={
          <TouchableOpacity onPress={() => setShow(true)}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text bold margin={[0, 5, 0]}>
                {address ? address.split(" ")[2] : "주소 선택"}
              </Text>
              <AntDesign name="caretdown" size={10} color="black" />
            </View>
          </TouchableOpacity>
        }
        right={
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={() => navigation.navigate("Search")}>
              <Image style={styles.searchIcon} source={search} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("ShareStack", {
                  screen: "Notification",
                })
              }
            >
              <Image style={styles.notificationIcon} source={bell} />
            </TouchableOpacity>
          </View>
        }
      ></Header>
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen
          name="내 주변 미션"
          navigation={navigation}
          children={(props) => (
            <PostTab type={"mission"} data={mission} {...props} />
          )}
        />
        <Tab.Screen
          name="내 주변 재능"
          navigation={navigation}
          children={(props) => (
            <MissionTab type={"talent"} data={talent} {...props} />
          )}
        />
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
  },
  searchIcon: {
    width: 22,
    height: 22,
    marginLeft: "auto",
    marginRight: 10,
  },
  notificationIcon: {
    width: 22,
    height: 22,
  },
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
  },
  uploadButton: {
    position: "absolute",
    bottom: 10,
    right: 10,
    zIndex: 100,
    padding: 14,
    backgroundColor: COLORS.main,
    borderRadius: 50,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  setLocaiton: {
    position: "absolute",
    bottom: 0,
    zIndex: 100,
    backgroundColor: "#fff",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
});
