import React, { useState } from "react";
import { StyleSheet, Platform, View, KeyboardAvoidingView } from "react-native";
import { Button, Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import HideWithKeyboard from "react-native-hide-with-keyboard";

import COLORS from "@modules/color";

// contexts
export default ({ navigation }) => {
  const [name, setName] = useState("");

  const [account, setAccount] = useState("");
  const [phone, setPhone] = useState("");

  return (
    <Container>
      <Header navigation={navigation} center={"결제정보"}></Header>
      <View style={styles.topLineContainer}>
        <View style={styles.topLineLeft}></View>
        <View style={styles.topLineRight}></View>
      </View>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <View>
          <Text s4 bold margin={[10, 0, 5]}>
            이름<Text danger>*</Text>
          </Text>
          <TextInput
            placeholder={"이름을 입력해주세요."}
            value={name}
            onChangeText={(value) => setName(value)}
          />

          <Text s4 bold margin={[30, 0, 5]}>
            계좌번호
          </Text>
          <TextInput
            placeholder={"계좌번호를 입력해주세요."}
            value={account}
            onChangeText={(value) => setAccount(value)}
          />
          <Text s4 bold margin={[30, 0, 5]}>
            연락처
          </Text>
          <TextInput
            placeholder={"연락처를 입력해주세요."}
            value={phone}
            onChangeText={(value) => setPhone(value)}
          />
        </View>
      </KeyboardAvoidingView>
      <HideWithKeyboard>
        <Button
          gray
          text="다음 단계로"
          style={{ marginTop: "auto", flex: 0, marginBottom: 0 }}
        ></Button>
      </HideWithKeyboard>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineLeft: { flex: 1, borderTopWidth: 3, borderTopColor: COLORS.main },
  topLineRight: {
    flex: 1,
    borderTopWidth: 3,
    borderTopColor: COLORS.lightGray,
  },
  dateContainer: {
    width: 150,
    height: 50,
    borderColor: COLORS.gray,
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 2,
  },
  dateIcon: { position: "absolute", left: 6, top: 13, width: 24, height: 24 },
  dateInput: {
    marginLeft: 30,
    borderWidth: 0,
    paddingTop: 10,
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  checkIcon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  row: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
  },
  rightRow: {
    marginLeft: "auto",
    flexDirection: "row",
    alignItems: "center",
  },
});
