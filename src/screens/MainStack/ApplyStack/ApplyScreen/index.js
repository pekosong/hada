import React, { useState } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Text, MainCard, Divider, Modal } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import COLORS from "@modules/color";

import { useSelector } from "react-redux";
import { selectApplication } from "@stores/application";

const selectRoundGray = require("@assets/icons/selectRoundGray.png");
const selectRoundYellow = require("@assets/icons/selectRoundYellow.png");
const down = require("@assets/icons/down.png");

const FilterIcon = ({ type, onPress, sortType }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.filterItem}>
        {type === "mission" ? (
          <Text s5 margin={[0, 5, 0, 0]}>
            {sortType === "" && "전체"}
            {sortType === "success" && "미션 성공"}
            {sortType === "process" && "수행 중"}
            {sortType === "deny" && "실패"}
          </Text>
        ) : (
          <Text s5 margin={[0, 5, 0, 0]}>
            {sortType === "" && "전체"}
            {sortType === "success" && "재능 수행 성공"}
            {sortType === "process" && "재능 수행 중"}
            {sortType === "deny" && "재능 수행 실패"}
          </Text>
        )}
        <Image source={down} />
      </View>
    </TouchableOpacity>
  );
};

const FirstTab = ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  const [sortType, setSortType] = useState("");

  const { applications } = useSelector(selectApplication);
  const data = applications
    .filter((el) => el.post.type === "mission")
    .sort((a, b) => b.create_at > a.create_at)
    .filter((el) => {
      if (sortType) return el.status === sortType;
      return el;
    });

  const handleClick = (item) => {
    navigation.navigate("ApplyDetail", { item });
  };

  return (
    <View style={{ flex: 1 }}>
      <Modal showModal={showModal} setShowModal={setShowModal}>
        <Text s2 bold margin={[0, 0, 20]}>
          미션 상태
        </Text>
        {[
          { sort: "", text: "전체" },
          { sort: "success", text: "미션 성공" },
          { sort: "process", text: "수행 중" },
          { sort: "deny", text: "실패" },
        ].map((el) => (
          <TouchableOpacity
            key={el.sort}
            onPress={() => {
              setSortType(el.sort);
              setShowModal(false);
            }}
          >
            <View style={styles.modalItem}>
              <Text>{el.text}</Text>
              <Image
                source={
                  sortType === el.sort ? selectRoundYellow : selectRoundGray
                }
              />
            </View>
          </TouchableOpacity>
        ))}
      </Modal>
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text>{`총 ${data.length}개`}</Text>
            <FilterIcon
              type={"mission"}
              sortType={sortType}
              onPress={() => {
                setShowModal(true);
              }}
            />
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={data}
        renderItem={({ item }) => {
          return (
            <MainCard
              item={item.post}
              onPress={() => handleClick(item)}
              applyStatus={item.status}
            ></MainCard>
          );
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const SecondTab = ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  const [sortType, setSortType] = useState("");

  const { applications } = useSelector(selectApplication);
  const data = applications
    .filter((el) => el.post.type === "talent")
    .sort((a, b) => b.create_at > a.create_at)
    .filter((el) => {
      if (sortType) return el.status === sortType;
      return el;
    });

  const handleClick = (item) => {
    navigation.navigate("ApplyDetail", { item });
  };

  return (
    <View style={{ flex: 1 }}>
      <Modal showModal={showModal} setShowModal={setShowModal}>
        <Text s2 bold margin={[0, 0, 20]}>
          미션 상태
        </Text>
        {[
          { sort: "", text: "전체" },
          { sort: "success", text: "재능 수행 성공" },
          { sort: "process", text: "재능 수행 중" },
          { sort: "deny", text: "재능 수행 실패" },
        ].map((el) => (
          <TouchableOpacity
            key={el.sort}
            onPress={() => {
              setSortType(el.sort);
              setShowModal(false);
            }}
          >
            <View style={styles.modalItem}>
              <Text>{el.text}</Text>
              <Image
                source={
                  sortType === el.sort ? selectRoundYellow : selectRoundGray
                }
              />
            </View>
          </TouchableOpacity>
        ))}
      </Modal>
      <FlatList
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <Text>{`총 ${data.length}개`}</Text>
            <FilterIcon
              type={"talent"}
              sortType={sortType}
              onPress={() => {
                setShowModal(true);
              }}
            />
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={data}
        renderItem={({ item }) => {
          return (
            <MainCard
              item={item.post}
              onPress={() => handleClick(item)}
              applyStatus={item.status}
            ></MainCard>
          );
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ navigation }) => {
  return (
    <Container top>
      <Header center={"지원목록"}></Header>
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen
          name="미션 지원 목록"
          component={FirstTab}
          navigation={navigation}
        />
        <Tab.Screen
          name="재능 지원 목록"
          component={SecondTab}
          navigation={navigation}
        />
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  modalItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
  filterItem: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 50,
    marginRight: 10,
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderColor: COLORS.playGray,
    backgroundColor: COLORS.playGray,
  },
});
