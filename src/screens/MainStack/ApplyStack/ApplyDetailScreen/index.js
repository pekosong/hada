import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { Text, Divider } from "@components";
import { ScrollView } from "react-native-gesture-handler";

import { useSelector, useDispatch } from "react-redux";
import { selectLike, setLikes } from "@stores/like";
import { selectAuth } from "@stores/auth";
import { selectApplication, setApplications } from "@stores/application";
import { removeApplication } from "@services/application";
import { changeProfile } from "@services/auth";
import { AntDesign } from "@expo/vector-icons";
import COLORS from "@modules/color";
import { perCategory } from "@constants/category";
import { getData, getDistanceFromLatLonInKm } from "@modules/util";
import { getPostById } from "@services/post";
import { isWriteReview } from "@services/review";

// contexts
export default ({ route, navigation }) => {
  const {
    item,
    item: { post },
  } = route.params;
  const {
    id,
    address,
    priceType,
    title,
    images,
    workTime,
    contents,
    lat,
    lng,
  } = post;

  const [distance, setDistance] = useState(item.distance || "");
  const [isWrite, setWrite] = useState(false);

  const dispatch = useDispatch();
  const { likes } = useSelector(selectLike);
  const { profile } = useSelector(selectAuth);
  const { applications } = useSelector(selectApplication);

  useEffect(() => {
    if (!item.distance) {
      getData("location").then((value) => {
        const { latitude, longitude } = value;
        setDistance(getDistanceFromLatLonInKm(lat, lng, latitude, longitude));
      });
    }
    isWriteReview({ post: post.id, id: profile.id }).then((el) =>
      setWrite(el.data.length !== 0)
    );
  }, [navigation]);

  const handleLike = async () => {
    const newlikes = likes.map((el) => el.id).includes(id)
      ? likes.filter((el) => el.id !== id)
      : likes.concat(post);
    await changeProfile({ id: profile.id, likes: newlikes });
    dispatch(setLikes(newlikes));
  };

  const handleCancel = async () => {
    try {
      await removeApplication(item.id);
      const newApplications = applications.filter((el) => el.id !== item.id);
      dispatch(setApplications(newApplications));
      navigation.goBack();
    } catch (err) {
      console.log(err);
    }
  };

  const handleClick = async () => {
    const { data } = await getPostById(post.id);
    navigation.navigate("ShareStack", {
      screen: "HomeDetail",
      params: { item: data },
    });
  };

  return (
    <Container>
      <Header navigation={navigation} center={"지원목록"}></Header>
      <ScrollView>
        <Divider width={5} padding={0} />
        <View style={styles.rowContainer}>
          <Text>지원내용</Text>
          {/* <TouchableOpacity style={{ padding: 10 }}>
            <AntDesign name="right" color="gray" size={12} />
          </TouchableOpacity> */}
        </View>
        <Divider width={2} padding={10} />
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            {images.length > 0 && (
              <Image source={{ uri: images[0]["url"] }} style={styles.image} />
            )}
            <View style={styles.contentWrapper}>
              {item.status === "apply" && (
                <Text s5 style={styles.apply}>
                  지원중
                </Text>
              )}
              {item.status === "process" && (
                <Text success s5 style={styles.success}>
                  진행중
                </Text>
              )}
              {item.status === "success" && (
                <Text success s5 style={styles.success}>
                  {post.type === "mission" ? "미션성공" : "재능성공"}
                </Text>
              )}
              {item.status === "failure" && (
                <Text danger s5 style={styles.failure}>
                  {post.type === "mission" ? "미션실패" : "재능실패"}
                </Text>
              )}
              {item.status === "deny" && (
                <Text danger s5 style={styles.failure}>
                  {post.type === "mission" ? "미션실패" : "재능실패"}
                </Text>
              )}
              <Text bold margin={[4, 0, 0]}>
                {title}
              </Text>
              <Text s4>일당 합의</Text>
            </View>
          </View>
        </View>
        <Divider width={5} padding={0} />
        <View style={styles.rowContainer}>
          <Text>상세정보</Text>
          <TouchableOpacity style={{ padding: 10 }} onPress={handleClick}>
            <AntDesign name="right" color="gray" size={12} />
          </TouchableOpacity>
        </View>
        <Divider width={2} padding={10} />
        <View style={[styles.container, { marginBottom: 10 }]}>
          <DetailItem
            title={"소요시간"}
            content={workTime === 0 ? "미정" : workTime}
          ></DetailItem>
          <DetailItem
            title={"입금형태"}
            content={perCategory[priceType]["text"]}
          ></DetailItem>
          <DetailItem title={"위치"} content={address}></DetailItem>
          <DetailItem
            title={"거리"}
            content={`${
              Number.parseFloat(distance) < 100
                ? Number.parseFloat(distance).toFixed(1) + "km 이내"
                : "100km 이상"
            }`}
          ></DetailItem>
          <DetailItem title={"내용"} content={contents}></DetailItem>
        </View>
        <Divider width={5} padding={0} />
        {/* <View style={styles.rowContainer}>
          <Text>결제정보</Text>
          <TouchableOpacity
            style={{ padding: 10 }}
            onPress={() => navigation.navigate("Payment")}
          >
            <AntDesign name="right" color="gray" size={12} />
          </TouchableOpacity>
        </View>
        <Divider width={2} padding={10} />
        <View style={[styles.container, { marginBottom: 10 }]}>
          <DetailItem title={"예금주"} content={"김와플"}></DetailItem>
          <DetailItem title={"계좌번호"} content={"일당"}></DetailItem>
          <DetailItem title={"연락처"} content={"010-9141-9090"}></DetailItem>
        </View>
        <Divider width={10} padding={0} /> */}
      </ScrollView>
      {item.status !== "deny" && (
        <View style={styles.bottomContainer}>
          <TouchableOpacity style={{ padding: 20 }} onPress={handleLike}>
            <AntDesign
              name={
                likes.map((el) => el.id).indexOf(id) === -1 ? "hearto" : "heart"
              }
              size={22}
              color={
                likes.map((el) => el.id).indexOf(id) === -1
                  ? COLORS.gray
                  : COLORS.danger
              }
            />
          </TouchableOpacity>
          <View style={styles.verticalDivider}></View>

          <TouchableOpacity style={styles.button} onPress={handleCancel}>
            <Text main center margin={[0, 5, 0]}>
              지원취소
            </Text>
          </TouchableOpacity>
          {item.status !== "apply" && item.status !== "deny" && (
            <TouchableOpacity
              style={[
                styles.button,
                {
                  backgroundColor: COLORS.main,
                },
              ]}
              onPress={() => {
                if (isWrite) return;
                navigation.navigate("Review", {
                  item: post,
                  callback: setWrite,
                });
              }}
            >
              <Text white center margin={[0, 5, 0]}>
                {isWrite ? "후기작성 완료" : "후기작성"}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      )}
    </Container>
  );
};

const DetailItem = ({ title, content }) => {
  return (
    <View style={styles.detailItemContainer}>
      <Text g1 s4 style={{ width: 100 }}>
        {title}
      </Text>
      <Text s4 style={{ flex: 1 }}>
        {content}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  detailItemContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5,
  },
  bottomContainer: {
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: COLORS.lightGray,
  },
  verticalDivider: {
    borderLeftWidth: 1,
    borderLeftColor: COLORS.lightGray,
    height: 40,
  },
  button: {
    flex: 1,
    height: "100%",
    justifyContent: "center",
  },
  contentContainer: { flexDirection: "row", marginBottom: 10 },
  contentWrapper: {
    marginLeft: 15,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  image: { width: 80, height: 80 },
  rowContainer: {
    height: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    paddingHorizontal: 10,
    alignItems: "center",
  },
  apply: {
    borderWidth: 1,
    borderColor: COLORS.black,
    paddingHorizontal: 4,
  },
  success: {
    borderWidth: 1,
    borderColor: COLORS.success,
    paddingHorizontal: 4,
  },
  failure: {
    borderWidth: 1,
    borderColor: COLORS.danger,
    paddingHorizontal: 4,
  },
});
