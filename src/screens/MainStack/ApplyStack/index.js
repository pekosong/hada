import React from "react";

import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import ApplyScreen from "./ApplyScreen";
import ApplyDetailScreen from "./ApplyDetailScreen";
import PaymentScreen from "./PaymentScreen";
import ReviewScreen from "./ReviewScreen";

const ApplyStack = createStackNavigator();
function ApplyStackNavigation() {
  return (
    <ApplyStack.Navigator headerMode="none">
      <ApplyStack.Screen
        name="Apply"
        component={ApplyScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ApplyStack.Screen
        name="ApplyDetail"
        component={ApplyDetailScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ApplyStack.Screen
        name="Payment"
        component={PaymentScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ApplyStack.Screen
        name="Review"
        component={ReviewScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </ApplyStack.Navigator>
  );
}

export default ApplyStackNavigation;
