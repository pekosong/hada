import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import { Button, Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import HideWithKeyboard from "react-native-hide-with-keyboard";
import { AirbnbRating } from "react-native-ratings";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { AntDesign } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import COLORS from "@modules/color";
import { useSelector } from "react-redux";
import { selectAuth } from "@stores/auth";
import { addReview } from "@services/review";
import { uploadImage } from "@services/image";

const check = require("@assets/icons/check.png");
const checkYellow = require("@assets/icons/checkYellow.png");

// contexts
export default ({ route, navigation }) => {
  const { item, callback } = route.params;
  const [rating, setRating] = useState(0);
  const [type1, setType1] = useState(false);
  const [type2, setType2] = useState(false);
  const [type3, setType3] = useState(false);
  const [type4, setType4] = useState(false);
  const [content, setContent] = useState("");
  const [imageList, setImageList] = useState([]);
  const [previewList, setPreviewList] = useState([]);
  const auth = useSelector(selectAuth);

  useEffect(() => {
    getPermissionAsync();
  }, []);

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  const _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
    });
    if (!result.cancelled) {
      const uri = result.uri;
      let uriParts = uri.split(".");
      let fileType = uriParts[uriParts.length - 1];
      let formData = new FormData();
      formData.append("files", {
        uri,
        name: `file.${fileType}`,
        type: `image/${fileType}`,
      });
      setImageList([...imageList, formData]);
      setPreviewList([...previewList, result.uri]);
    }
  };

  const handleRemove = (idx) => {
    let newPreview = [...previewList];
    newPreview.splice(idx, 1);
    setPreviewList(newPreview);
  };

  const handleConfirm = async () => {
    try {
      if (rating && content && (type1 || type2 || type3 || type4)) {
        const values = await Promise.all(
          imageList.map((image) => uploadImage(image))
        );
        const data = {
          post: item.id,
          host: item.user,
          guest: auth.profile.id,
          type: item.type,
          rating: rating,
          contents: content,
          defaultReason1: type1,
          defaultReason2: type2,
          defaultReason3: type3,
          defaultReason4: type4,
          images: values.length ? values : null,
        };
        await addReview({ data });
        Alert.alert("안내", "성공하였습니다.", [
          {
            text: "확인",
            onPress: () => {
              callback(true);
              navigation.goBack();
            },
          },
        ]);
      }
    } catch (err) {
      Alert.alert("안내", "실패하였습니다.\n내용을 다시 확인하세요", [
        { text: "확인" },
      ]);
    }
  };

  return (
    <Container>
      <Header navigation={navigation} center={"후기 작성"} border></Header>
      <KeyboardAwareScrollView
        style={styles.container}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <View>
          <Text center s4 bold margin={[10, 0, 20]}>
            평점을 입력해주세요.
          </Text>
          <AirbnbRating
            showRating={false}
            count={5}
            defaultRating={0}
            size={35}
            onFinishRating={(rating) => setRating(rating)}
          />
          <Text center s4 bold margin={[40, 0, 10]}>
            미션이 좋았던 이유를 선택해주세요.
          </Text>
          <SelectItem
            active={type1}
            text={"친절하고 매너가 좋아요."}
            onPress={() => setType1(!type1)}
          />
          <SelectItem
            active={type2}
            text={"미션이 설명한 것과 같아요."}
            onPress={() => setType2(!type2)}
          />
          <SelectItem
            active={type3}
            text={"미션설명이 자세해요."}
            onPress={() => setType3(!type3)}
          />
          <SelectItem
            active={type4}
            text={"미션의 가격이 적당해요."}
            onPress={() => setType4(!type4)}
          />
          <Text center s4 bold lines={4} margin={[40, 0, 10]}>
            상세 리뷰를 작성해주세요.
          </Text>
          <Text s4 g1 style={{ textAlign: "right" }} margin={[0, 0, 5]}>
            <Text s4>{`${content.length}자`}</Text>
            {`/100자`}
          </Text>
          <TextInput
            placeholder={"상세한 리뷰를 작성해주세요."}
            value={content}
            onChangeText={(value) => {
              if (value.length > 100) return;
              setContent(value);
            }}
            multiline
            numberOfLines={4}
          />
          <Text center s4 bold lines={4} margin={[40, 0, 10]}>
            사진을 등록해주세요.(선택)
          </Text>
          <View style={{ marginBottom: 30, flexDirection: "row" }}>
            <TouchableOpacity
              style={styles.uploadButton}
              onPress={() => {
                if (previewList.length === 4) return;
                _pickImage();
              }}
            >
              <View>
                <AntDesign name="camerao" size={22} color="gray" />
                <Text center s5 g1>
                  {`${previewList.length}/4`}
                </Text>
              </View>
            </TouchableOpacity>
            {previewList.length > 0 &&
              previewList.map((el, idx) => (
                <TouchableOpacity key={idx} style={styles.cameraContainer}>
                  <Image source={{ uri: el }} style={styles.image} />
                  <TouchableOpacity
                    onPress={() => handleRemove(idx)}
                    style={styles.closeBtnContainer}
                  >
                    <AntDesign name="close" size={14} color="white" />
                  </TouchableOpacity>
                </TouchableOpacity>
              ))}
          </View>
        </View>
      </KeyboardAwareScrollView>
      <HideWithKeyboard>
        <Button
          main={rating && content && (type1 || type2 || type3 || type4)}
          text="작성완료"
          style={{ marginTop: "auto", flex: 0, marginBottom: 0 }}
          onPress={handleConfirm}
        ></Button>
      </HideWithKeyboard>
    </Container>
  );
};

const SelectItem = ({ active, text, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={{ marginVertical: 8 }}>
      <View style={styles.row}>
        <Image style={styles.checkIcon} source={active ? checkYellow : check} />
        <Text s4>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  checkIcon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  uploadButton: {
    width: 70,
    height: 70,
    backgroundColor: COLORS.playGray,
    alignItems: "center",
    justifyContent: "center",
  },
  cameraContainer: {
    marginLeft: 5,
    width: 70,
    height: 70,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  image: {
    borderColor: COLORS.lightGray,
    borderWidth: 1,
    height: "100%",
    width: "100%",
  },
  closeBtnContainer: {
    position: "absolute",
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "black",
    top: -10,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },
});
