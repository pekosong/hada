import React from "react";

import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import HomeDetailScreen from "./HomeDetailScreen";
import MessageDetailScreen from "./MessageDetailScreen";
import ProfileScreen from "./ProfileScreen";
import NotificationScreen from "./NotificationScreen";
import PaymentScreen from "./PaymentScreen";
import ReviewScreen from "./ReviewScreen";
import ReportScreen from "./ReportScreen";
import AddScreen from "./AddScreen";

const ShareStack = createStackNavigator();
function ShareStackNavigation() {
  return (
    <ShareStack.Navigator headerMode="none">
      <ShareStack.Screen
        name="Add"
        component={AddScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="HomeDetail"
        component={HomeDetailScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="MessageDetail"
        component={MessageDetailScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="Notification"
        component={NotificationScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="Payment"
        component={PaymentScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="Review"
        component={ReviewScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
      <ShareStack.Screen
        name="Report"
        component={ReportScreen}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </ShareStack.Navigator>
  );
}

export default ShareStackNavigation;
