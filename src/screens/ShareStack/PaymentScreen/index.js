import React, { useReducer } from "react";
import { StyleSheet, Platform, View, ScrollView, Alert } from "react-native";
import { Button, Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";

import KeyboardSpacer from "react-native-keyboard-spacer";

import COLORS from "@modules/color";
import { getSize } from "@modules/util";
import { useSelector, useDispatch } from "react-redux";
import { updateProfile, selectAuth } from "@stores/auth";
import { addPayment, changePayment } from "@services/payment";

const { width } = getSize();

const reducer = (state, action) => {
  switch (action.type) {
    case "update":
      const errors = validateValue(action);
      return { ...state, [action.name]: action.value, ...errors };
    default:
      return state;
  }
};

const validateValue = (action) => {
  let errors = {};
  if (action.name === "name") {
    const korRe = /^[가-힣]+$/;
    if (!korRe.test(action.value)) {
      errors["nameErr"] = "이름을 정확히 입력하세요";
    } else if (action.value.length < 2) {
      errors["nameErr"] = "이름을 정확히 입력하세요";
    } else {
      errors["nameErr"] = "";
    }
  }
  if (action.name === "phoneNumber") {
    const re = /^\d*\.?\d*$/;
    const validNumber = re.test(action.value);
    if (!validNumber) {
      errors["phoneErr"] = "핸드폰 번호를 정확히 입력하세요.";
    } else if (action.value.length !== 11) {
      errors["phoneErr"] = "핸드폰 번호를 정확히 입력하세요.";
    } else {
      errors["phoneErr"] = "";
    }
  }
  return errors;
};

// contexts
export default ({ route, navigation }) => {
  const { item, handle } = route.params;
  const authDispatch = useDispatch();
  const auth = useSelector(selectAuth);
  const initState = {
    name: auth.profile ? auth.profile.name : "",
    phoneNumber: auth.profile ? auth.profile.phoneNumber : "",
    bankName: "",
    accountNumber: "",
  };
  const [state, dispatch] = useReducer(reducer, item || initState);
  const {
    name,
    phoneNumber,
    bankName,
    accountNumber,
    nameErr,
    phoneErr,
  } = state;

  const handleChange = ({ name, value }) => {
    dispatch({ type: "update", name, value });
  };

  const handleConfirmAuth = () => {
    handle({ bankName, accountNumber });
    navigation.goBack();
  };

  const handleConfirm = async () => {
    if (
      !name ||
      !bankName ||
      !accountNumber ||
      !phoneNumber ||
      nameErr ||
      phoneErr
    ) {
      Alert.alert("안내", "값을 확인하세요", [
        { text: "확인", onPress: () => console.log("OK Pressed") },
      ]);
      return;
    }
    const data = {
      user: auth.profile.id,
      name,
      bankName,
      accountNumber,
      phoneNumber,
    };
    try {
      if (item) {
        await changePayment({ id: item.id, data });
        data["id"] = item.id;
      } else {
        const result = await addPayment({ data });
        data["id"] = result.data.id;
      }
      authDispatch(updateProfile({ name: "payment", value: data }));
      navigation.goBack();
    } catch (err) {
      console.log(err.response.data);
    }
  };
  return (
    <Container>
      <Header navigation={navigation} center={"기본정보"}></Header>
      <View style={styles.topLineContainer}>
        <View style={styles.topLineLeft}></View>
        <View style={styles.topLineRight}></View>
      </View>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps="handled"
      >
        <View style={{ padding: 10 }}>
          {!handle && (
            <>
              <Text s4 bold margin={[10, 0, 10]}>
                예금주
              </Text>
              <TextInput
                placeholder={"이름을 입력해주세요."}
                value={name}
                onChangeText={(value) => handleChange({ name: "name", value })}
                err={nameErr}
              />
              {!!nameErr && (
                <Text s4 danger margin={[0, 5, 10]}>
                  {nameErr}
                </Text>
              )}
            </>
          )}
          <Text s4 bold margin={[handle ? 10 : 30, 0, 10]}>
            은행명
          </Text>
          <TextInput
            placeholder={"은행명을 입력해주세요."}
            value={bankName}
            onChangeText={(value) => handleChange({ name: "bankName", value })}
          />
          <Text s4 bold margin={[30, 0, 10]}>
            계좌번호
          </Text>
          <TextInput
            placeholder={"계좌번호를 입력해주세요."}
            value={accountNumber}
            onChangeText={(value) =>
              handleChange({ name: "accountNumber", value })
            }
          />
          {!handle && (
            <>
              <Text s4 bold margin={[30, 0, 10]}>
                연락처
              </Text>
              <TextInput
                placeholder={"연락처를 입력해주세요."}
                value={phoneNumber}
                onChangeText={(value) =>
                  handleChange({ name: "phoneNumber", value })
                }
                err={phoneErr}
              />
              {!!phoneErr && (
                <Text s4 danger margin={[0, 5, 10]}>
                  {phoneErr}
                </Text>
              )}
            </>
          )}
          <View style={{ height: 30 }}></View>
        </View>
        {Platform.OS === "ios" && <KeyboardSpacer />}
      </ScrollView>
      <Button
        main={
          handle
            ? bankName && accountNumber
            : name &&
              bankName &&
              accountNumber &&
              phoneNumber &&
              !nameErr &&
              !phoneErr
        }
        text="저장하기"
        style={{ flex: 0, height: 60 }}
        onPress={handle ? handleConfirmAuth : handleConfirm}
      ></Button>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {},
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineLeft: { flex: 1, borderTopWidth: 3, borderTopColor: COLORS.main },
  topLineRight: {
    flex: 1,
    borderTopWidth: 3,
    borderTopColor: COLORS.lightGray,
  },
  dateContainer: {
    width: width * 0.4,
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: 2,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
  },
  dateIcon: { width: 22, height: 22, marginHorizontal: 10 },
  dateInput: {
    marginLeft: 30,
    borderWidth: 0,
    paddingTop: 10,
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  checkIcon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  row: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
  },
  rightRow: {
    marginLeft: "auto",
    flexDirection: "row",
    alignItems: "center",
  },
  nickBtn: {
    borderWidth: 1,
    borderColor: COLORS.gray,
    height: 50,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
  },
});
