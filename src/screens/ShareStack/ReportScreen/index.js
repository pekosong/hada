import React, { useState } from "react";
import { StyleSheet, Platform, View } from "react-native";
import { Button, Text, TextInput } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import HideWithKeyboard from "react-native-hide-with-keyboard";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { useSelector } from "react-redux";
import { selectAuth } from "@stores/auth";
import { addReview } from "@services/review";

// contexts
export default ({ route, navigation }) => {
  const [content, setContent] = useState("");

  const auth = useSelector(selectAuth);

  const handleConfirm = async () => {
    const data = {};
    await addReview({ data });
    navigation.goBack();
  };

  return (
    <Container>
      <Header navigation={navigation} center={"신고하기"} border></Header>
      <KeyboardAwareScrollView
        style={styles.container}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <View>
          <TextInput
            placeholder={"신고사유를 작성해주세요."}
            value={content}
            onChangeText={(value) => {
              if (value.length > 100) return;
              setContent(value);
            }}
            multiline
            numberOfLines={8}
            style={{ height: 200 }}
          />
          <Text s4 g1 style={{ textAlign: "right" }} margin={[5, 0, 0]}>
            <Text s4>{`${content.length}자`}</Text>
            {`/1000자`}
          </Text>
        </View>
      </KeyboardAwareScrollView>
      <HideWithKeyboard>
        <View style={{ padding: 20 }}>
          <Text g1 s5>
            주의사항{"\n"}-신고시, 해당사항을 확인후 반영됩니다.{"\n"}
            -신고된 유저는 3개월간 활동이 금지됩니다.
          </Text>
        </View>
        <Button
          main={content}
          text="보내기"
          style={{ marginTop: "auto", flex: 0, marginBottom: 0 }}
          onPress={handleConfirm}
        ></Button>
      </HideWithKeyboard>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  closeBtnContainer: {
    position: "absolute",
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "black",
    top: -10,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },
});
