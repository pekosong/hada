import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { Text } from "@components";
import { useSelector } from "react-redux";

import { selectAuth } from "@stores/auth";
import {
  getNotifications,
  setNotificationsRead,
  removeAllNotifications,
} from "@services/notification";
import { fromNow } from "@modules/util";

const setting = require("@assets/icons/setting.png");
const avatar = require("@assets/images/avatar.png");
const hada = require("@assets/icons/app_icon.jpg");

// contexts
export default ({ navigation }) => {
  const [notifications, setNotifications] = useState([]);
  const { profile } = useSelector(selectAuth);

  useEffect(() => {
    (async () => {
      const { data } = await getNotifications({ user: profile.id });
      setNotifications(data.filter((el) => el.post));
      const notReadIds = data.filter((el) => !el.see).map((el) => el.id);
      if (notReadIds.length) setNotificationsRead(notReadIds);
    })();
  }, []);

  const handleRemoveNotifications = () => {
    removeAllNotifications(notifications.map((el) => el.id));
    setNotifications([]);
  };

  return (
    <Container>
      <Header
        navigation={navigation}
        border
        center={"알림"}
        right={
          <TouchableOpacity onPress={handleRemoveNotifications}>
            <Image style={styles.notificationIcon} source={setting} />
          </TouchableOpacity>
        }
      />
      <FlatList
        style={styles.container}
        data={notifications}
        renderItem={({ item }) => {
          return <NotificationCard item={item} />;
        }}
        keyExtractor={(item) => item.id.toString()}
      />
    </Container>
  );
};

const NotificationCard = ({ item, onPress }) => {
  const { post, user } = item;
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[
          styles.cardContainer,
          !item.see && { backgroundColor: COLORS.lightMain },
        ]}
      >
        <Image
          source={user.profile ? { uri: user.profile["url"] } : avatar}
          style={styles.image}
        ></Image>
        <View style={{ flex: 1, paddingRight: 10 }}>
          <Text s5 margin={[0, 0, 0]}>
            {item.contents}
          </Text>
          <Text g1 s6>
            {fromNow(item.created_at)}
          </Text>
        </View>
        <Image
          source={
            post.images && post.images.length > 0
              ? { uri: post.images[0]["url"] }
              : hada
          }
          style={styles.postImage}
        ></Image>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: "row",
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 40,
    marginRight: 10,
  },
  postImage: {
    width: 40,
    height: 40,
  },
});
