import React from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { Text, Divider, ReviewCard } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import COLORS from "@modules/color";
import { AntDesign } from "@expo/vector-icons";

import { calcAvg } from "@modules/util";
import { useSelector } from "react-redux";
import { selectAuth } from "@stores/auth";

const FirstTab = ({ navigation, reviews }) => {
  const avgRating = calcAvg(reviews.map((el) => el.rating));
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        style={{ paddingHorizontal: 10 }}
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <AntDesign name="star" size={20} color={COLORS.main} />
            <Text
              bold
              margin={[0, 0, 0, 5]}
            >{`${avgRating}점 (후기 ${reviews.length}개)`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => <ReviewCard item={item}></ReviewCard>}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const SecondTab = ({ navigation, reviews }) => {
  const avgRating = calcAvg(reviews.map((el) => el.rating));
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        style={{ paddingHorizontal: 10 }}
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <AntDesign name="star" size={20} color={COLORS.main} />
            <Text
              bold
              margin={[0, 0, 0, 5]}
            >{`${avgRating}점 (후기 ${reviews.length}개)`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => <ReviewCard item={item}></ReviewCard>}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const ThirdTab = ({ navigation, reviews }) => {
  const avgRating = calcAvg(reviews.map((el) => el.rating));
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        style={{ paddingHorizontal: 10 }}
        ListHeaderComponent={() => (
          <View style={styles.listHeader}>
            <AntDesign name="star" size={20} color={COLORS.main} />
            <Text
              bold
              margin={[0, 0, 0, 5]}
            >{`${avgRating}점 (후기 ${reviews.length}개)`}</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <Divider width={1}></Divider>}
        data={reviews}
        renderItem={({ item }) => <ReviewCard item={item}></ReviewCard>}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

const Tab = createMaterialTopTabNavigator();

// contexts
export default ({ route, navigation }) => {
  const { reviews } = route.params;
  const auth = useSelector(selectAuth);
  const data = reviews ?? auth.profile.reviews;
  return (
    <Container top style={{ flex: 1 }}>
      <Header navigation={navigation} center="받은 후기"></Header>
      <Tab.Navigator
        tabBarOptions={{
          indicatorStyle: { backgroundColor: COLORS.main },
        }}
        backBehavior={"none"}
      >
        <Tab.Screen name="전체후기">
          {(props) => <FirstTab {...props} reviews={data} />}
        </Tab.Screen>
        <Tab.Screen name="미션후기">
          {(props) => (
            <SecondTab
              {...props}
              reviews={data.filter((el) => el.type === "mission")}
            />
          )}
        </Tab.Screen>
        <Tab.Screen name="재능후기">
          {(props) => (
            <ThirdTab
              {...props}
              reviews={data.filter((el) => el.type === "talent")}
            />
          )}
        </Tab.Screen>
      </Tab.Navigator>
    </Container>
  );
};

const styles = StyleSheet.create({
  listHeader: {
    paddingHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 10,
    flexDirection: "row",
    alignItems: "center",
  },
});
