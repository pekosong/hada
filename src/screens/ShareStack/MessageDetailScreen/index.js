import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { Text } from "@components";
import { GiftedChat, Send, Bubble } from "react-native-gifted-chat";
import { AntDesign } from "@expo/vector-icons";
import firestore from "@react-native-firebase/firestore";
import COLORS from "@modules/color";
import { setChatRead, updateDocument } from "@modules/firebase";
import { covertPrice } from "@modules/util";
import { useSelector } from "react-redux";
import { selectAuth } from "@stores/auth";
import { getPostById } from "@services/post";

import { useDispatch } from "react-redux";
import { changeMessages } from "@stores/message";
import { perCategory } from "@constants/category";
require("dayjs/locale/ko");

const avatar =
  "https://hada2020.s3.ap-northeast-2.amazonaws.com/avatar_2484248f5a.png";

// contexts
export default ({ route, navigation }) => {
  const dispatch = useDispatch();
  const { doc, postId } = route.params;

  const [active, setActive] = useState(false);
  const [item, setItem] = useState(null);
  const [messages, setMessages] = useState([]);

  const auth = useSelector(selectAuth);
  const { id, name, profile } = auth.profile;

  useEffect(() => {
    setChatRead({ doc, id: id.toString() });
    const unsubscribe = firestore()
      .collection("messages")
      .doc(doc)
      .onSnapshot((document) => {
        if (document.data()["messages"].length) {
          const messagesFirestore = document
            .data()
            ["messages"].map((el) => ({
              ...el,
              createdAt: el.createdAt.toDate(),
            }))
            .sort((a, b) => b.createdAt - a.createdAt);
          setMessages(messagesFirestore);
          const { text, createdAt } = messagesFirestore[0];
          dispatch(changeMessages({ id: document.id, text, createdAt }));
        } else {
          setMessages([]);
        }
      });
    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    const getItem = async () => {
      const { data } = await getPostById(postId);
      setItem(data);
    };
    getItem();
  }, []);

  async function handleSend(newMsg) {
    const { _id, text, createdAt } = newMsg[0];
    const msg = {
      _id,
      text,
      createdAt,
      read: [id.toString()],
      user: {
        _id: id.toString(),
        name,
        avatar: profile ? profile["url"] : avatar,
      },
    };
    await updateDocument({ doc, messages: [...messages, msg] });
  }

  const handleItem = () => {
    navigation.push("HomeDetail", { item });
  };

  return (
    <Container>
      <Header navigation={navigation} border></Header>
      <View style={{ height: 76 }}>
        {item && (
          <TouchableOpacity style={styles.container} onPress={handleItem}>
            {item.images.length > 0 && (
              <Image
                source={{ uri: item.images[0]["url"] }}
                style={styles.image}
              ></Image>
            )}
            <View>
              <Text g1 s4>
                {item.title}
              </Text>
              <Text g1 s4>
                {`${perCategory[item.priceType]["text"]} ${covertPrice(
                  item.price
                )}`}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
      <GiftedChat
        user={{
          _id: id.toString(),
          name,
        }}
        alwaysShowSend={true}
        placeholder="메세지를 입력해주세요"
        messages={messages}
        locale={"ko"}
        onSend={handleSend}
        onInputTextChanged={(value) => {
          setActive(value ? true : false);
        }}
        renderBubble={(props) => {
          return (
            <Bubble
              {...props}
              textStyle={{
                right: {
                  color: COLORS.black,
                },
              }}
              wrapperStyle={{
                left: {
                  backgroundColor: COLORS.playGray,
                },
                right: {
                  backgroundColor: COLORS.main,
                },
              }}
            />
          );
        }}
        renderSend={(props) => (
          <Send {...props}>
            <View
              style={[styles.sand, active && { backgroundColor: COLORS.main }]}
            >
              <AntDesign name="arrowup" size={20} color="white" />
            </View>
          </Send>
        )}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 15,
    alignItems: "center",
    backgroundColor: COLORS.white,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 2,
  },
  image: { width: 45, height: 45, marginRight: 10 },
  sand: {
    marginRight: 10,
    marginBottom: 8,
    width: 30,
    height: 30,
    borderRadius: 30,
    backgroundColor: COLORS.playGray,
    alignItems: "center",
    justifyContent: "center",
  },
});
