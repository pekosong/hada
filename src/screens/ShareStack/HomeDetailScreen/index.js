import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, TouchableOpacity, Alert } from "react-native";
import Container from "@layouts/container";
import Header from "@layouts/header";
import { SliderBox } from "react-native-image-slider-box";
import { Text, Divider, Message, Loader } from "@components";

import { ScrollView } from "react-native-gesture-handler";
import { useSelector, useDispatch } from "react-redux";
import { AntDesign } from "@expo/vector-icons";

import COLORS from "@modules/color";

import { getDocument, addDocument, getAllDocument } from "@modules/firebase";
import { covertPrice, convertDay, calcRating } from "@modules/util";

import { perCategory } from "@constants/category";
import { selectAuth } from "@stores/auth";
import { selectLike, setLikes } from "@stores/like";
import { selectApplication, setApplications } from "@stores/application";
import { setMessages } from "@stores/message";

import { changeProfile } from "@services/auth";
import { addApplication } from "@services/application";
import { getUserReviews } from "@services/review";

const calendarYellow = require("@assets/icons/calendarYellow.png");
const clock = require("@assets/icons/clock.png");

const avatar = require("@assets/images/avatar.png");
const starFillYellow = require("@assets/icons/starFillYellow.png");

// contexts
export default ({ route, navigation }) => {
  const { item } = route.params;
  const {
    id,
    address,
    distance,
    title,
    images,
    viewCount,
    likers,
    startDay,
    endDay,
    startTime,
    endTime,
    user,
    workTime,
    contents,
    price,
    priceType,
  } = item;

  const [reviews, setReviews] = useState([]);
  useEffect(() => {
    const setData = async () => {
      const data = await getUserReviews(user.id);
      setReviews(data.data);
    };
    setData();
  }, []);

  const [modal, setModal] = useState(false);

  const dispatch = useDispatch();
  const { likes } = useSelector(selectLike);
  const { applications } = useSelector(selectApplication);
  const { profile } = useSelector(selectAuth);

  const [likeCnt, setLikeCnt] = useState(likers.length);

  const handleLike = async () => {
    let newlikes;
    if (likes.map((el) => el.id).includes(id)) {
      newlikes = likes.filter((el) => el.id !== id);
      setLikeCnt(likeCnt - 1);
    } else {
      newlikes = likes.concat(item);
      setLikeCnt(likeCnt + 1);
    }

    await changeProfile({ id: profile.id, likes: newlikes });
    dispatch(setLikes(newlikes));
  };

  const handleApplication = async () => {
    if (applications.map((el) => el.post.id).includes(id)) {
      Alert.alert("안내", "이미 지원하였습니다.", [
        { text: "확인", onPress: () => console.log("OK Pressed") },
      ]);
    } else {
      const data = {
        user: profile.id,
        post: id,
      };
      const result = await addApplication(data);
      const newApplications = applications.concat(result.data);
      dispatch(setApplications(newApplications));
      setModal(true);
    }
  };

  const handleChat = async () => {
    const data = {
      post: id,
      host: user.id,
      hostDeviceToken: user.deviceToken,
      user: profile.id,
      userDeviceToken: profile.deviceToken,
    };
    const result = await getDocument(data);
    if (result.docs.length) {
      navigation.navigate("MessageDetail", {
        doc: result.docs[0].id,
        postId: id.toString(),
      });
    } else {
      const docRef = await addDocument(data);
      const chatList = await getAllDocument({ id: profile.id });
      dispatch(setMessages(chatList));
      navigation.navigate("MessageDetail", {
        doc: docRef.id,
        postId: id.toString(),
      });
    }
  };

  const icon = perCategory[priceType];
  if (!profile) return <Loader></Loader>;
  return (
    <Container>
      {modal && <Message message={"지원하였습니다."} callback={setModal} />}
      <Header navigation={navigation} center={"상세내용"}></Header>
      <ScrollView>
        <SliderBox
          sliderBoxHeight={200}
          images={images.map((el) => el["url"])}
          paginationBoxVerticalPadding={20}
          dotColor="#FFF"
          inactiveDotColor="#AAA"
          dotStyle={{
            width: 5,
            height: 5,
            margin: -2,
          }}
        />
        <View style={styles.container}>
          <Text bold s2 margin={[10, 0, 5]}>
            {title}
          </Text>
          <View style={styles.badgeContainer}>
            <Text g1>{`${address} ${distance ?? "거리 미정"}`}</Text>
            <View style={[styles.badgeContainer, { marginLeft: "auto" }]}>
              <AntDesign name="eyeo" size={14} color={COLORS.gray} />
              <Text g1 margin={[0, 15, 0, 5]}>
                {viewCount}
              </Text>
            </View>
            <View style={styles.badgeContainer}>
              <AntDesign name="hearto" size={14} color={COLORS.gray} />
              <Text g1 margin={[0, 0, 0, 5]}>
                {likeCnt}
              </Text>
            </View>
          </View>
        </View>
        <Divider width={1} padding={20} />
        <View style={[styles.container, { flexDirection: "row" }]}>
          <View style={styles.iconContainer}>
            <View style={styles.iconWrapper}>
              <View
                style={{
                  ...styles.icon,
                  backgroundColor: icon.bgColor,
                }}
              >
                <Text s4 style={{ color: icon.textColor }}>
                  {icon.text}
                </Text>
              </View>
            </View>
            <Text s4 margin={[5, 0, 0]}>
              {covertPrice(price)}
            </Text>
          </View>
          <View style={styles.iconContainer}>
            <View style={styles.iconWrapper}>
              <Image source={calendarYellow} />
            </View>
            <Text s4 margin={[5, 0, 0]}>
              {`${convertDay(startDay)} - ${convertDay(endDay)}`}
            </Text>
          </View>
          <View style={styles.iconContainer}>
            <View style={styles.iconWrapper}>
              <Image source={clock} />
            </View>
            <Text s4 margin={[5, 0, 0]}>
              {`${startTime.slice(0, 5)} - ${endTime.slice(0, 5)}`}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate("Profile", { user })}
        >
          <View style={styles.profileContainer}>
            <Image
              source={user.profile ? { uri: user.profile["url"] } : avatar}
              style={styles.avatar}
            />
            <Text g1 s4 margin={[0, 0, 0, 10]}>
              {user.nickname || user.name}
            </Text>
            <View style={styles.starContainer}>
              <Image source={starFillYellow} style={{ marginTop: 5 }} />
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text center bold>
                  {calcRating(reviews)}
                </Text>
                <Text center g1 s5>
                  신뢰도
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <Divider width={10} padding={20} />
        <Text center bold>
          상세정보
        </Text>
        <Divider width={1} padding={20} />
        <View style={styles.container}>
          <DetailItem title={"소요시간"} content={workTime}></DetailItem>
          <DetailItem title={"입금형태"} content={icon["text"]}></DetailItem>
          <DetailItem title={"위치"} content={address}></DetailItem>
          <DetailItem
            title={"거리"}
            content={distance ?? "거리 미정"}
          ></DetailItem>
          <DetailItem title={"내용"} content={contents}></DetailItem>
        </View>
        <Divider width={1} padding={20} />
        <TouchableOpacity
          style={styles.informContainer}
          onPress={() => navigation.navigate("Report")}
        >
          <Text g1 s4 margin={[0, 5, 2]}>
            신고하기
          </Text>
          <AntDesign name="right" size={14} color={COLORS.gray} />
        </TouchableOpacity>
        <Divider width={10} padding={0} />
      </ScrollView>
      {user.id !== profile.id && (
        <View style={styles.bottomContainer}>
          <TouchableOpacity style={{ padding: 20 }} onPress={handleLike}>
            <AntDesign
              name={
                likes.map((el) => el.id).indexOf(id) === -1 ? "hearto" : "heart"
              }
              size={22}
              color={
                likes.map((el) => el.id).indexOf(id) === -1
                  ? COLORS.gray
                  : COLORS.danger
              }
            />
          </TouchableOpacity>
          <View style={styles.verticalDivider}></View>
          <TouchableOpacity style={styles.button} onPress={handleChat}>
            <Text main center margin={[0, 5, 0]}>
              채팅하기
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.button,
              {
                backgroundColor: COLORS.main,
              },
            ]}
            onPress={handleApplication}
          >
            <Text white center margin={[0, 5, 0]}>
              {applications.map((el) => el.post.id).includes(id)
                ? "지원 중"
                : "지원하기"}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </Container>
  );
};

const DetailItem = ({ title, content }) => {
  return (
    <View style={styles.detailItemContainer}>
      <Text g1 s4 style={{ width: 100 }}>
        {title}
      </Text>
      <Text s4 style={{ flex: 1 }}>
        {content}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  badgeContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    borderBottomColor: COLORS.gray,
    borderBottomWidth: 10,
  },
  iconContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  iconWrapper: {
    height: 50,
    justifyContent: "center",
  },
  profileContainer: {
    borderWidth: 1,
    borderColor: COLORS.gray,
    borderRadius: 15,
    paddingHorizontal: 10,
    marginTop: 20,
    height: 70,
    margin: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  starContainer: {
    marginLeft: "auto",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  detailItemContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5,
  },
  informContainer: {
    flexDirection: "row",
    paddingBottom: 20,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingHorizontal: 10,
  },
  bottomContainer: {
    height: 60,
    flexDirection: "row",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: COLORS.lightGray,
  },
  verticalDivider: {
    borderLeftWidth: 1,
    borderLeftColor: COLORS.lightGray,
    height: 40,
  },
  button: {
    flex: 1,
    height: "100%",
    justifyContent: "center",
  },
  icon: {
    padding: 13,
    borderRadius: 30,
  },
  avatar: { width: 30, height: 30, borderRadius: 30 },
});
