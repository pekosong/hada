import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from "react-native";
import { Text, Divider, ReviewCard } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";

import COLORS from "@modules/color";
import { AirbnbRating } from "react-native-ratings";
import { getUserPosts } from "@services/post";
import { getUserReviews } from "@services/review";

import { getSize, calcAvg } from "@modules/util";
import { AntDesign } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";

const { width } = getSize();

const avatar = require("@assets/images/avatar.png");
const hada = require("@assets/icons/app_icon.jpg");

// contexts
export default ({ route, navigation }) => {
  const { user } = route.params;
  const [posts, setPosts] = useState([]);
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    const setData = async () => {
      const [data1, data2] = await Promise.all([
        getUserPosts(user.id),
        getUserReviews(user.id),
      ]);
      setPosts(data1.data);
      setReviews(data2.data);
    };
    setData();
  }, []);

  const rating = calcAvg(reviews.map((el) => el.rating));

  const handlePressPost = async (item) => {
    navigation.push("HomeDetail", { item });
  };

  return (
    <Container>
      <Header
        navigation={navigation}
        center={"프로필"}
        border
        right={
          <TouchableOpacity
            onPress={() => navigation.navigate("Report")}
            style={{ paddingLeft: 10, paddingVertical: 10 }}
          >
            <Text g1 s5>
              신고하기
            </Text>
          </TouchableOpacity>
        }
      />
      <ScrollView>
        <TouchableOpacity style={styles.avatarContainer}>
          <Image
            source={user.profile ? { uri: user.profile["url"] } : avatar}
            style={styles.image}
          />
        </TouchableOpacity>
        <Text center s4 bold margin={[5, 0, 10]}>
          {!!user.nickname ? user.nickname : user.name}
        </Text>
        <View style={styles.ratingContainer}>
          <AirbnbRating
            showRating={false}
            count={5}
            defaultRating={rating}
            size={15}
          />
          <Text margin={[0, 0, 0, 5]}>{rating}</Text>
        </View>
        <Divider />
        <View style={styles.header}>
          <Text bold s4>
            {`등록한 게시글 ${posts.length}개`}
          </Text>
          <TouchableOpacity>
            <AntDesign name="right" size={15} color={COLORS.gray} />
          </TouchableOpacity>
        </View>
        <FlatList
          data={posts}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <PostCard
              item={item}
              first={index === 0}
              onPress={() => handlePressPost(item)}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
        <Divider />
        <View style={styles.header}>
          <Text bold s4>
            {`받은 후기 ${reviews.length}개`}
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("Review", { reviews })}
          >
            <AntDesign name="right" size={15} color={COLORS.gray} />
          </TouchableOpacity>
        </View>
        <View style={{ paddingHorizontal: 10 }}>
          {reviews.map((item, index) => {
            return (
              <View key={index}>
                <ReviewCard item={item} />
                {index !== 2 && <Divider width={1} />}
              </View>
            );
          })}
        </View>
        <Divider />
        <View style={styles.header}>
          <Text bold s4>
            받은 평가
          </Text>
          <TouchableOpacity>
            <AntDesign name="right" size={15} color={COLORS.gray} />
          </TouchableOpacity>
        </View>
        <View style={{ paddingHorizontal: 10, marginBottom: 20 }}>
          <ReputationCard
            icon={"🙋‍♀️"}
            count={reviews.filter((el) => el.defaultReason1).length}
            text={"친절하고 매너가 좋아요"}
          />
          <ReputationCard
            icon={"🙆‍♂️"}
            count={reviews.filter((el) => el.defaultReason2).length}
            text={"미션이 설명한 것과 같아요"}
          />
          <ReputationCard
            icon={"📝"}
            count={reviews.filter((el) => el.defaultReason3).length}
            text={"미션설명이 자세해요"}
          />
          <ReputationCard
            icon={"💵"}
            count={reviews.filter((el) => el.defaultReason4).length}
            text={"친절하고 매너가 좋아요"}
          />
        </View>
      </ScrollView>
    </Container>
  );
};

const PostCard = ({ first, item, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[
          styles.postContainer,
          {
            marginLeft: first ? 10 : 0,
          },
        ]}
      >
        <Image
          style={styles.postImage}
          source={
            item.images.length > 0 ? { uri: item.images[0]["url"] } : hada
          }
        />
        <Text s6 margin={[5, 0, 0]}>
          {item.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const ReputationCard = ({ icon, count, text }) => {
  return (
    <View style={styles.reputationContainer}>
      <Text style={{ width: 50 }}>{icon}</Text>
      <Text style={{ width: 50 }}>{count + "명"}</Text>
      <Text g1 style={styles.descText}>
        {text}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineRight: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: COLORS.lightGray,
  },
  avatarContainer: {
    alignItems: "center",
    paddingVertical: 10,
  },
  ratingContainer: { flexDirection: "row", justifyContent: "center" },
  image: {
    width: 70,
    height: 70,
    borderRadius: 70,
  },
  header: {
    paddingHorizontal: 10,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  postContainer: { width: width * 0.25, marginRight: 10 },
  postImage: { width: width * 0.25, height: width * 0.25 },
  reputationContainer: { flexDirection: "row", paddingVertical: 5 },
  descText: { backgroundColor: COLORS.playGray, paddingHorizontal: 10 },
});
