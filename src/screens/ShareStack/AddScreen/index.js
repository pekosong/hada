import React, { useState, useEffect, useReducer } from "react";
import {
  StyleSheet,
  Platform,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import { Button, Text, TextInput, ModalWebView } from "@components";
import Container from "@layouts/container";
import Header from "@layouts/header";
import HideWithKeyboard from "react-native-hide-with-keyboard";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { AntDesign } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import COLORS from "@modules/color";
import DropDownPicker from "react-native-dropdown-picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { addPost, updatePost } from "@services/post";
import { uploadImage } from "@services/image";

import { getSize, convertTime, diffTime, getData } from "@modules/util";
import { useSelector, useDispatch } from "react-redux";
import { selectAuth } from "@stores/auth";
import { appendPost, modifyPost } from "@stores/post";
import { storeData } from "@modules/util";

const { width } = getSize();

const calendar = require("@assets/icons/Calendar.png");
const clockGray = require("@assets/icons/clockGray.png");

const checkBox = require("@assets/icons/checkBox.png");
const checkBoxYellow = require("@assets/icons/checkBoxYellow.png");
const location = require("@assets/icons/location.png");

const initValue = {
  type: null,
  title: null,
  priceType: null,
  price: null,
  priceErr: null,
  startDay: null,
  endDay: null,
  startTime: null,
  endTime: null,
  applicant: null,
  address: null,
  lat: null,
  lng: null,
  contents: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "update":
      const errors = validateValue(action);
      return { ...state, [action.name]: action.value, ...errors };
    default:
      return state;
  }
};

const validateValue = (action) => {
  let errors = {};
  if (action.name === "price") {
    const numberRe = /^\d+$/;
    if (action.value === "" && !numberRe.test(action.value)) {
      errors["priceErr"] = "가격을 정확히 입력하세요";
    } else {
      errors["priceErr"] = null;
    }
  }
  return errors;
};

// contexts
export default ({ route, navigation }) => {
  const postDispatch = useDispatch();
  const { reloadData, item } = route.params;
  const [isShow, setShow] = useState(false);
  const [nego, setNego] = useState(false);
  const [mode, setMode] = useState("date");
  const [dateType, setDateType] = useState("startDay");
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [imageList, setImageList] = useState(
    item ? item.images.map((el) => ({ id: el.id, url: el.url })) : []
  );
  const [previewList, setPreviewList] = useState(
    item ? item.images.map((el) => ({ id: el.id, url: el.url })) : []
  );
  const [state, dispatch] = useReducer(
    reducer,
    item
      ? {
          ...item,
          startTime: item.startTime.slice(0, 5),
          endTime: item.endTime.slice(0, 5),
        }
      : initValue
  );
  const {
    type,
    title,
    priceType,
    price,
    priceErr,
    startDay,
    endDay,
    startTime,
    endTime,
    applicant,
    address,
    lat,
    lng,
    contents,
  } = state;

  const isActive =
    type &&
    title &&
    priceType &&
    startDay &&
    endDay &&
    startTime &&
    endTime &&
    applicant &&
    contents &&
    !priceErr &&
    (price || nego);

  const auth = useSelector(selectAuth);

  useEffect(() => {
    (async () => {
      if (!address) {
        const location = await getData("location");
        const address = await getData("address");
        const { latitude, longitude } = location;
        handleChange({ name: "lat", value: latitude });
        handleChange({ name: "lng", value: longitude });
        handleChange({ name: "address", value: address });
      }
      getPermissionAsync();
    })();
  }, []);

  const handleConfirm = (date) => {
    if (dateType === "startDay" || dateType === "endDay") {
      const value = moment(date).format("YYYY-MM-DD");
      handleChange({ name: dateType, value });
    } else {
      const value = moment(date).format("LT");
      handleChange({ name: dateType, value });
    }
    setDatePickerVisibility(false);
  };

  const handleChange = ({ name, value }) => {
    if (name === "type") {
      dispatch({ type: "update", name: "PriceType", value: null });
      controller.reset();
    }
    dispatch({ type: "update", name, value });
  };

  const getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  const _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
    });
    if (!result.cancelled) {
      const uri = result.uri;
      let uriParts = uri.split(".");
      let fileType = uriParts[uriParts.length - 1];

      let formData = new FormData();
      formData.append("files", {
        uri,
        name: `file.${fileType}`,
        type: `image/${fileType}`,
      });
      setImageList([...imageList, formData]);
      setPreviewList([...previewList, { url: result.uri }]);
    }
  };
  const handleRemove = (idx) => {
    let newPreview = [...previewList];
    newPreview.splice(idx, 1);
    setPreviewList(newPreview);
    let newImage = [...imageList];
    newImage.splice(idx, 1);
    setImageList(newImage);
  };

  const handleAdd = async () => {
    if (!isActive) return;
    try {
      if (item) {
        const workTime = diffTime(startTime, endTime);
        let newStartTime = startTime;
        let newEndTime = endTime;
        if (item.startTime.slice(0, 5) !== startTime) {
          newStartTime = convertTime(startTime);
        } else {
          newStartTime = startTime + ":00.000";
        }
        if (item.endTime.slice(0, 5) !== endTime) {
          newEndTime = convertTime(endTime);
        } else {
          newEndTime = endTime + ":00.000";
        }
        const data = {
          title,
          priceType,
          price: price || 0,
          startDay,
          endDay,
          startTime: newStartTime,
          endTime: newEndTime,
          workTime,
          applicant,
          address,
          lat,
          lng,
          contents,
        };
        const upload = imageList.filter((el) => !el.id);
        const current = imageList.filter((el) => el.id).map((el) => el.id);
        const values = await Promise.all(
          upload.map((image) => uploadImage(image))
        );
        data["images"] = [...current, ...values];
        const result = await updatePost({ id: item.id, data });
        postDispatch(modifyPost(result.data));
      } else {
        const workTime = diffTime(startTime, endTime);
        const data = {
          type,
          title,
          priceType,
          price: price || 0,
          startDay,
          endDay,
          startTime: startTime && convertTime(startTime),
          endTime: endTime && convertTime(endTime),
          workTime,
          applicant,
          address,
          lat,
          lng,
          contents,
          status: "recruiting",
          user: auth.profile.id,
        };
        const values = await Promise.all(
          imageList.map((image) => uploadImage(image))
        );
        if (values) data["images"] = values;
        const result = await addPost({ data });
        postDispatch(appendPost(result.data));
        reloadData();
      }
      Alert.alert("안내", "성공하였습니다.", [
        { text: "확인", onPress: () => navigation.goBack() },
      ]);
    } catch (e) {
      Alert.alert("안내", "실패하였습니다.\n내용을 다시 확인하세요", [
        { text: "확인" },
      ]);
      // console.log(e.response.data);
    }
  };

  let controller;

  return (
    <Container>
      <ModalWebView
        showModal={isShow}
        setShowModal={setShow}
        lat={lat}
        lng={lng}
        onMessage={async (msg) => {
          const [address, latitude, longitude] = msg.nativeEvent.data.split(
            ","
          );
          setShow(false);
          if (address) {
            handleChange({ name: "address", value: address });
            handleChange({ name: "lat", value: latitude });
            handleChange({ name: "lng", value: longitude });
            await storeData({
              key: "location",
              value: { latitude, longitude },
            });
            await storeData({ key: "address", value: address });
          }
        }}
      />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={mode}
        onConfirm={handleConfirm}
        onCancel={() => setDatePickerVisibility(false)}
      />
      <Header navigation={navigation} center={"등록하기"}></Header>
      <View style={styles.topLineContainer}>
        <View style={styles.topLineRight}></View>
      </View>
      <KeyboardAwareScrollView
        style={styles.container}
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <TouchableOpacity
              style={styles.uploadButton}
              onPress={() => {
                if (previewList.length === 4) return;
                _pickImage();
              }}
            >
              <View>
                <AntDesign name="camerao" size={22} color="gray" />
                <Text center s5 g1>
                  {`${previewList.length}/4`}
                </Text>
              </View>
            </TouchableOpacity>
            {previewList.length > 0 &&
              previewList.map((el, idx) => (
                <TouchableOpacity key={idx} style={styles.cameraContainer}>
                  <Image source={{ uri: el["url"] }} style={styles.image} />
                  <TouchableOpacity
                    onPress={() => handleRemove(idx)}
                    style={styles.closeBtnContainer}
                  >
                    <AntDesign name="close" size={14} color="white" />
                  </TouchableOpacity>
                </TouchableOpacity>
              ))}
          </View>
          <Space />
          {!item && (
            <View style={styles.row}>
              <Button
                lightMain={type === "mission"}
                outline={type !== "mission"}
                textGray={type !== "mission"}
                text="미션"
                onPress={() => handleChange({ name: "type", value: "mission" })}
              />
              <View style={{ width: 5 }}></View>
              <Button
                lightMain={type === "talent"}
                outline={type !== "talent"}
                textGray={type !== "talent"}
                text="재능"
                onPress={() => handleChange({ name: "type", value: "talent" })}
              />
            </View>
          )}
          <Space />
          <TextInput
            placeholder={"제목을 입력해주세요."}
            value={title}
            onChangeText={(value) => {
              if (value.length > 100) return;
              handleChange({ name: "title", value });
            }}
          />
          <Space />
          <DropDownPicker
            controller={(instance) => (controller = instance)}
            style={styles.dropContainer}
            placeholderStyle={{
              color: COLORS.gray,
            }}
            containerStyle={{ width: width * 0.5 }}
            placeholder={"임금형태"}
            items={
              type === "mission"
                ? [
                    { label: "일당", value: "perDay" },
                    { label: "건당", value: "perCase" },
                    { label: "시급", value: "perHour" },
                  ]
                : [{ label: "1인당 금액", value: "perPerson" }]
            }
            defaultValue={priceType}
            onChangeItem={(item) =>
              handleChange({ name: "priceType", value: item.value })
            }
          />
          <Space />
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TextInput
              keyboardType={"number-pad"}
              style={{ flex: 1, marginVertical: 0 }}
              placeholder={"금액을 제시해주세요."}
              value={price}
              onChangeText={(value) => {
                setNego(false);
                handleChange({ name: "price", value });
              }}
            />
            <TouchableOpacity
              onPress={() => {
                setNego((nego) => !nego);
                handleChange({ name: "price", value: null });
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Image
                  source={nego ? checkBoxYellow : checkBox}
                  style={styles.dateIcon}
                ></Image>
                <Text s4 margin={[0, 0, 0, 0]}>
                  협의 후 결정
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {!!priceErr && (
            <Text s4 danger margin={[5, 5, 10]}>
              {priceErr}
            </Text>
          )}
          <Space />
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => {
                setMode("date");
                setDateType("startDay");
                setDatePickerVisibility(true);
              }}
            >
              <View style={styles.dateContainer}>
                <Image source={calendar} style={styles.dateIcon}></Image>
                <Text s4 g2>
                  {startDay || "미션수행일"}
                </Text>
              </View>
            </TouchableOpacity>
            <View style={styles.center}>
              <Text g1>~</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                setMode("date");
                setDateType("endDay");
                setDatePickerVisibility(true);
              }}
            >
              <View style={styles.dateContainer}>
                <Image source={calendar} style={styles.dateIcon}></Image>
                <Text s4 g2>
                  {endDay || "미션종료일"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Space />
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => {
                setMode("time");
                setDateType("startTime");
                setDatePickerVisibility(true);
              }}
            >
              <View style={styles.dateContainer}>
                <Image source={clockGray} style={styles.dateIcon}></Image>
                <Text s4 g2>
                  {startTime || "시작시간"}
                </Text>
              </View>
            </TouchableOpacity>
            <View style={styles.center}>
              <Text g1>~</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                setMode("time");
                setDateType("endTime");
                setDatePickerVisibility(true);
              }}
            >
              <View style={styles.dateContainer}>
                <Image source={clockGray} style={styles.dateIcon}></Image>
                <Text s4 g2>
                  {endTime || "종료시간"}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Space />
          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <TextInput
              keyboardType={"number-pad"}
              style={{ width: width * 0.5, marginVertical: 0 }}
              placeholder={"최대 지원자 수"}
              value={applicant && applicant.toString()}
              onChangeText={(value) => {
                if (value.length > 100) return;
                handleChange({ name: "applicant", value });
              }}
            />
            <Text s4 margin={[0, 0, 0, 5]}>
              명
            </Text>
          </View>
          <Space />
          <TouchableOpacity onPress={() => setShow(true)}>
            <View style={styles.locationContainer}>
              <Image
                source={location}
                style={{ width: 20, height: 20 }}
              ></Image>
              <Text margin={[0, 0, 0, 5]}>{address}</Text>
              <Text g1 s4 style={{ marginLeft: "auto" }}>
                위치변경
              </Text>
            </View>
          </TouchableOpacity>
          <Space />
          <TextInput
            placeholder={
              "아래 내용을 입력해주세요.\n- 시간\n- 장소\n- 내용\n- 기타"
            }
            value={contents}
            onChangeText={(value) => {
              if (value.length > 500) return;
              handleChange({ name: "contents", value });
            }}
            multiline
            numberOfLines={10}
            style={{ height: 180, marginBottom: 50 }}
          />
        </View>
      </KeyboardAwareScrollView>
      <HideWithKeyboard>
        <View style={styles.bottomContainer}>
          {!item && <Button normal textMain text="이전 정보 불러오기"></Button>}
          <Button
            main={isActive}
            textWhite={isActive}
            gray
            text={item ? "수정완료" : "등록완료"}
            onPress={handleAdd}
          ></Button>
        </View>
      </HideWithKeyboard>
    </Container>
  );
};

const Space = () => <View style={styles.space} />;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  topLineContainer: { display: "flex", flexDirection: "row" },
  topLineRight: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: COLORS.lightGray,
  },
  icon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  checkIcon: {
    width: 22,
    height: 22,
    marginRight: 10,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  uploadButton: {
    width: 70,
    height: 70,
    backgroundColor: COLORS.playGray,
    alignItems: "center",
    justifyContent: "center",
  },
  cameraContainer: {
    marginLeft: 5,
    width: 70,
    height: 70,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
  },
  image: {
    borderColor: COLORS.lightGray,
    borderWidth: 1,
    height: "100%",
    width: "100%",
  },
  closeBtnContainer: {
    position: "absolute",
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "black",
    top: -10,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  dropContainer: {
    borderWidth: 1,
    borderColor: COLORS.gray,
    width: width * 0.5,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
  space: { height: 15 },
  dateContainer: {
    width: width * 0.4,
    borderWidth: 1,
    borderColor: COLORS.gray,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
  },
  locationContainer: {
    flex: 1,
    borderWidth: 1,
    borderColor: COLORS.gray,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  dateIcon: { width: 22, height: 22, marginHorizontal: 10 },
  dateInput: {
    marginLeft: 30,
    borderWidth: 0,
    paddingTop: 10,
  },
  center: { flex: 1, justifyContent: "center", alignItems: "center" },
  bottomContainer: {
    flexDirection: "row",
    backgroundColor: COLORS.white,
    shadowColor: "#000",
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 2,
  },
});
