import React from "react";
import { View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useTheme } from "@react-navigation/native";

// contexts
export default ({ children, top }) => {
  const { colors } = useTheme();
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <SafeAreaView edges={top && ["top"]} style={{ flex: 1 }}>
        <View style={{ flex: 1, backgroundColor: colors.white }}>
          {children}
        </View>
      </SafeAreaView>
    </View>
  );
};
