import React from "react";
import { StyleSheet, TouchableOpacity, View, StatusBar } from "react-native";
import Text from "@components/Text";
import COLORS from "@modules/color";
import { AntDesign } from "@expo/vector-icons";

// contexts
export default ({ navigation, left, center, right, border, main }) => {
  const goBack = () => {
    navigation.goBack();
  };
  return (
    <View
      style={[
        styles.container,
        main && { backgroundColor: COLORS.main },
        border && { borderBottomWidth: 1, borderBottomColor: COLORS.playGray },
      ]}
    >
      <View style={styles.left}>
        {left && left}
        {navigation && (
          <TouchableOpacity onPress={goBack}>
            <AntDesign
              name="left"
              size={20}
              color={main ? "white" : "#333"}
              style={styles.button}
            />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.center}>
        <Text white={main}>{center}</Text>
      </View>
      <View style={styles.right}>{right}</View>
      {/* <StatusBar backgroundColor={main && COLORS.main} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    height: 50,
    paddingHorizontal: 10,
    justifyContent: "space-between",
    alignItems: "center",
  },
  left: {
    flex: 1,
  },
  center: {
    flex: 2,
    alignItems: "center",
  },
  right: {
    flex: 1,
    alignItems: "flex-end",
  },
});
