import React, { useState, useEffect } from "react";
import { StyleSheet, View, ActivityIndicator, Image } from "react-native";

import { NavigationContainer, useTheme } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import AuthStackNavigation from "@screens/AuthStack";
import HomeStackNavigation from "@screens/MainStack/HomeStack";
import ApplyStackNavigation from "@screens/MainStack/ApplyStack";
import MessageStackNavigation from "@screens/MainStack/MessageStack";
import MyStackNavigation from "@screens/MainStack/MyStack";
import ShareStackNavigation from "@screens/ShareStack";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import COLORS from "@modules/color";
import { requestUserPermission } from "@modules/util";
import * as SplashScreen from "expo-splash-screen";
import messaging from "@react-native-firebase/messaging";

import { getData, storeData, removeData } from "@modules/util";
import { useSelector, useDispatch } from "react-redux";

import { setLogin, setProfile, selectAuth } from "@stores/auth";
import { setLikes } from "@stores/like";
import { setApplications } from "@stores/application";
import { setMessages } from "@stores/message";
import { setPosts } from "@stores/post";

import { getAllDocument, updateToken } from "@modules/firebase";

import { getMyApplications } from "@services/application";
import { getProfile, setDeviceToken } from "@services/auth";
import { getMyPosts } from "@services/post";

import useMessage from "@hooks/useMessage";

import { Message } from "@components";

const Home = require("@assets/icons/selectHome.png");
const Apply = require("@assets/icons/selectApply.png");
const Chat = require("@assets/icons/selectMessage.png");
const My = require("@assets/icons/selectMy.png");

const DisabledHome = require("@assets/icons/disabledHome.png");
const DisabledApply = require("@assets/icons/disabledApply.png");
const DisabledMessage = require("@assets/icons/disabledMessage.png");
const DisabledMy = require("@assets/icons/disabledMy.png");

const MainTab = createBottomTabNavigator();

getTabBarVisibility = (route, hideList) => {
  const routeName = route.state
    ? route.state.routes[route.state.index].name
    : "";
  return hideList.includes(routeName) ? false : true;
};

function MainTabNavigation() {
  const dispatch = useDispatch();
  const { colors } = useTheme();

  useEffect(() => {
    requestUserPermission();
    const getSetAuth = async () => {
      const token = await getData("token");
      try {
        const result = await getProfile();
        const user = result.data;
        const deviceToken = await messaging().getToken();
        storeData({ key: "userId", value: user.id.toString() });
        storeData({ key: "deviceToken", value: deviceToken });

        const chatList = await getAllDocument({ id: user.id });
        const posts = await getMyPosts();
        const applications = await getMyApplications();

        setDeviceToken({ id: user.id, deviceToken });
        updateToken();
        dispatch(setLikes(user.likes));
        dispatch(setApplications(applications));
        dispatch(setPosts(posts));
        dispatch(setProfile({ token, profile: user }));

        dispatch(setMessages(chatList));
      } catch (err) {
        console.log(err);
        await removeData("token");
        await removeData("userId");
        await removeData("deviceToken");
        await removeData("address");
        await removeData("location");
      }
    };
    getSetAuth();
  }, []);

  return (
    <MainTab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let icon;
          if (route.name === "HomeStack") {
            icon = focused ? Home : DisabledHome;
          } else if (route.name === "ApplyStack") {
            icon = focused ? Apply : DisabledApply;
          } else if (route.name === "MessageStack") {
            icon = focused ? Chat : DisabledMessage;
          } else if (route.name === "MyStack") {
            icon = focused ? My : DisabledMy;
          }
          return <Image style={{ width: 30, height: 30 }} source={icon} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.black,
        inactiveTintColor: colors.gray,
      }}
    >
      <MainTab.Screen
        name="HomeStack"
        component={HomeStackNavigation}
        options={({ route }) => {
          return {
            tabBarVisible: getTabBarVisibility(route, ["Add"]),
            tabBarLabel: "홈",
          };
        }}
      />
      <MainTab.Screen
        name="ApplyStack"
        component={ApplyStackNavigation}
        options={({ route }) => {
          return {
            tabBarVisible: getTabBarVisibility(route, [
              "ApplyDetail",
              "Payment",
              "Review",
            ]),
            tabBarLabel: "지원목록",
          };
        }}
      />
      <MainTab.Screen
        name="MessageStack"
        component={MessageStackNavigation}
        options={({ route }) => {
          return {
            tabBarVisible: getTabBarVisibility(route, []),
            tabBarLabel: "메시지",
          };
        }}
      />
      <MainTab.Screen
        name="MyStack"
        component={MyStackNavigation}
        options={({ route }) => {
          return {
            tabBarVisible: getTabBarVisibility(route, []),
            tabBarLabel: "마이페이지",
          };
        }}
      />
    </MainTab.Navigator>
  );
}
const Stack = createStackNavigator();

export default () => {
  const [isLoading, setLoading] = useState(true);
  const [modal, setModal] = useState(false);
  const [message, setMessage] = useState(null);
  const auth = useSelector(selectAuth);
  const dispatch = useDispatch();
  useMessage(auth.profile, setModal, setMessage);
  useEffect(() => {
    getData("token").then((value) => {
      if (value) {
        dispatch(setLogin());
      }
      setLoading(false);
    });
    setTimeout(async () => {
      await SplashScreen.hideAsync();
    }, 2000);
  }, []);

  const MyTheme = {
    dark: false,
    colors: {
      primary: "rgb(255, 45, 85)",
      background: "rgba(255, 255, 255)",
      card: "rgb(255, 255, 255)",
      text: "rgb(28, 28, 30)",
      border: "rgb(199, 199, 204)",
      notification: "rgb(255, 69, 58)",
      ...COLORS,
    },
  };

  if (isLoading)
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="large" color="#fff"></ActivityIndicator>
      </View>
    );

  return (
    <View style={styles.continaer}>
      {modal && <Message message={message} callback={setModal} />}
      <NavigationContainer theme={MyTheme}>
        <Stack.Navigator headerMode="none">
          {!auth.isLogin ? (
            <Stack.Screen
              name="AuthStack"
              component={AuthStackNavigation}
              options={{
                ...TransitionPresets.ScaleFromCenterAndroid,
              }}
            />
          ) : (
            <Stack.Screen
              name="MainTab"
              component={MainTabNavigation}
              options={{
                ...TransitionPresets.ScaleFromCenterAndroid,
              }}
            />
          )}
          <Stack.Screen
            name="ShareStack"
            component={ShareStackNavigation}
            options={{
              ...TransitionPresets.SlideFromRightIOS,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
};

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.main,
  },
  continaer: { flex: 1, backgroundColor: "#fff" },
});
