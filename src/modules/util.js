import { Dimensions } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import messaging from "@react-native-firebase/messaging";

import moment from "moment";
import "moment/locale/ko";

export const calcRating = (reviews) => {
  if (reviews.length === 0) return 0;
  const ratings = reviews.map((el) => el.rating);
  return (ratings.reduce((a, b) => a + b, 0) / ratings.length).toFixed(1);
};

export async function requestUserPermission() {
  const authorizationStatus = await messaging().requestPermission({
    sound: false,
    announcement: true,
    badge: true,
  });

  if (authorizationStatus === messaging.AuthorizationStatus.AUTHORIZED) {
  } else if (
    authorizationStatus === messaging.AuthorizationStatus.PROVISIONAL
  ) {
    console.log("User has provisional notification permissions.");
  } else {
    console.log("User has notification permissions disabled");
  }
}

export const storeData = async ({ key, value }) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    console.log(e);
  }
};

export const getData = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    console.log(e);
  }
};

export const removeData = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    console.log(e);
  }
};

export const convertDay = (day) => {
  let newDay = `${moment(day).format("MM-DD")}`;
  if (newDay.startsWith("0")) {
    newDay = newDay.slice(1);
  }
  return newDay;
};

export const convertTime = (value) => {
  const result = value.replace("오후", "12").replace("오전", "0");
  const timeType = result.split(" ")[0];

  const [hour_, min_] = result.split(" ")[1].split(":");
  const hour = parseInt(hour_ === "12" ? 0 : timeType) + parseInt(hour_);
  return `${hour < 10 ? "0" + hour : hour}:${min_}:00.000`;
};

export const diffTime = (start, end) => {
  if (start.startsWith("오전 ")) {
    start = start.replace("오전 ", "");
  }
  if (start.startsWith("오후 ")) {
    start = start.replace("오후 ", "");
    var arr = start.split(":");
    start = `${parseInt(arr[0]) + 12}:${arr[1]}`;
  }
  if (end.startsWith("오전 ")) {
    end = end.replace("오전 ", "");
  }
  if (end.startsWith("오후 ")) {
    end = end.replace("오후 ", "");
    var arr = end.split(":");
    end = `${parseInt(arr[0]) + 12}:${arr[1]}`;
  }
  start = start.split(":");
  end = end.split(":");
  var startDate = new Date(0, 0, 0, start[0], start[1], 0);
  var endDate = new Date(0, 0, 0, end[0], end[1], 0);
  var diff = endDate.getTime() - startDate.getTime();
  var hours = Math.floor(diff / 1000 / 60 / 60);
  diff -= hours * 1000 * 60 * 60;
  var minutes = Math.floor(diff / 1000 / 60);

  // If using time pickers with 24 hours format, add the below line get exact hours
  if (hours < 0) hours = hours + 24;

  return hours;
};

export const numberWithCommas = (x) => {
  if (x === null) return;
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const getSize = () => Dimensions.get("window");

export const covertPrice = (price) => {
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원";
};

export const fromNow = (date) => {
  return moment(date).fromNow();
};

export const duration = (item) => {
  return `${moment(item.startDay).format("MM-DD")} ${item.startTime.slice(
    0,
    5
  )}-${item.endTime.slice(0, 5)}`;
};

export const calcAvg = (arr) => {
  if (arr.length === 0) return 0;
  return arr.reduce((a, b) => a + b, 0) / arr.length;
};

export const getDistanceFromLatLonInKm = (lat1, lon1, lat2, lon2) => {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
};

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}
