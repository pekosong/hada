export default COLORS = {
  main: "#ffcc21",
  lightMain: "#fffae8",
  point: "#fffae8",
  black: "#000000",
  darkGray: "#707070",
  gray: "#c6c6c6",
  lightGray: "#e6e6e6",
  playGray: "#f5f5f5",
  white: "#ffffff",
  success: "#24bc00",
  danger: "#ff4500",
};
