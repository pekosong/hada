import firestore from "@react-native-firebase/firestore";
import { getData } from "@modules/util";

export const getDocument = ({ post, host, user }) => {
  return firestore()
    .collection("messages")
    .where("postId", "==", post.toString())
    .where("host", "==", host.toString())
    .where("user", "==", user.toString())
    .get();
};

export const updateDocument = ({ doc, messages }) => {
  firestore().collection("messages").doc(doc).update({
    messages,
  });
};

export const updateToken = async () => {
  const userId = await getData("userId");
  const deviceToken = await getData("deviceToken");
  const msgRef = firestore().collection("messages");
  const host = msgRef.where("host", "==", userId).get();
  const user = msgRef.where("user", "==", userId).get();
  const [receiverQuerySnapshot, senderQuerySnapshot] = await Promise.all([
    host,
    user,
  ]);
  const capitalCitiesArray = receiverQuerySnapshot.docs;
  const italianCitiesArray = senderQuerySnapshot.docs;
  const citiesArray = capitalCitiesArray.concat(italianCitiesArray);
  citiesArray.forEach(function (doc) {
    if (doc.data()["host"] === userId) {
      doc.ref.update({ hostDeviceToken: deviceToken });
    } else {
      doc.ref.update({ userDeviceToken: deviceToken });
    }
  });
};

export const addDocument = ({
  post,
  host,
  hostDeviceToken,
  user,
  userDeviceToken,
}) => {
  return firestore().collection("messages").add({
    host: host.toString(),
    hostDeviceToken,
    user: user.toString(),
    userDeviceToken,
    messages: [],
    postId: post.toString(),
  });
};

export const getAllDocument = async ({ id }) => {
  const msgRef = firestore().collection("messages");
  const host = msgRef.where("host", "==", id.toString()).get();
  const user = msgRef.where("user", "==", id.toString()).get();
  const [receiverQuerySnapshot, senderQuerySnapshot] = await Promise.all([
    host,
    user,
  ]);
  const capitalCitiesArray = receiverQuerySnapshot.docs;
  const italianCitiesArray = senderQuerySnapshot.docs;
  const citiesArray = capitalCitiesArray.concat(italianCitiesArray);
  let chatList = [];
  citiesArray.forEach(function (doc) {
    const { postId, messages, host, user } = doc.data();
    if (messages.length) {
      const msg = messages.sort(
        (a, b) => b.createdAt.toDate() - a.createdAt.toDate()
      )[0];
      const { createdAt, text } = msg;
      const unRead = messages.filter((el) => !el.read.includes(id.toString()))
        .length;
      chatList.push({
        createdAt,
        text,
        id: doc.id,
        postId,
        host,
        user,
        unRead,
      });
    }
  });
  chatList = chatList
    .sort((a, b) => b.createdAt.toDate() - a.createdAt.toDate())
    .map((el) => ({ ...el, createdAt: el.createdAt.toDate() }));
  return chatList;
};

// export const setChatList = async ({ doc, id }) => {
//   const data = await firestore().collection("messages").doc(doc).get();
//   const messages = data.data()["messages"];
//   const newMessages = messages.map((el) => {
//     let read = el.read;
//     if (!read.includes(id)) {
//       read.push(id);
//     }
//     return { ...el, read };
//   });
//   data.ref.update({ messages: newMessages });
//   return await getAllDocument({ id });
// };

export const setChatRead = async ({ doc, id }) => {
  const data = await firestore().collection("messages").doc(doc).get();
  const messages = data.data()["messages"];
  const newMessages = messages.map((el) => {
    let read = el.read;
    if (!read.includes(id)) {
      read.push(id);
    }
    return { ...el, read };
  });
  data.ref.update({ messages: newMessages });
};
