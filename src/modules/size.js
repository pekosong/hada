export default SIZES = {
  s1: 28,
  s2: 24,
  s3: 16,
  s4: 14,
  s5: 12,
  s6: 11,
  s7: 10,
};
