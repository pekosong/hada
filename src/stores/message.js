import { createSlice } from "@reduxjs/toolkit";

export const messageSlice = createSlice({
  name: "message",
  initialState: {
    messages: [],
  },
  reducers: {
    setMessages: (state, action) => {
      state.messages = action.payload;
    },
    changeMessages: (state, action) => {
      let msg = state.messages.filter((el) => el.id === action.payload.id)[0];
      const index = state.messages.findIndex(
        (el) => el.id === action.payload.id
      );
      const newMessages = [...state.messages];
      newMessages[index] = { ...msg, ...action.payload, unRead: 0 };
      state.messages = newMessages;
    },
  },
});

export const {
  setMessages,
  changeMessages,
  setReadMessage,
} = messageSlice.actions;

export const selectMessage = (state) => state.message;

export default messageSlice.reducer;
