import { createSlice } from "@reduxjs/toolkit";

export const likeSlice = createSlice({
  name: "like",
  initialState: {
    likes: [],
  },
  reducers: {
    setLikes: (state, action) => {
      state.likes = action.payload;
    },
  },
});

export const { setLikes } = likeSlice.actions;

export const selectLike = (state) => state.like;

export default likeSlice.reducer;
