import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth";
import likeReducer from "./like";
import applicationReducer from "./application";
import messageReducer from "./message";
import postReducer from "./post";

export default configureStore({
  reducer: {
    auth: authReducer,
    like: likeReducer,
    application: applicationReducer,
    message: messageReducer,
    post: postReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
    }),
});
