import { createSlice } from "@reduxjs/toolkit";

export const postSlice = createSlice({
  name: "post",
  initialState: {
    posts: [],
  },
  reducers: {
    setPosts: (state, action) => {
      state.posts = action.payload;
    },
    appendPost: (state, action) => {
      state.posts.push(action.payload);
    },
    removePost: (state, action) => {
      state.posts = state.posts.filter((el) => el.id !== action.payload);
    },
    modifyPost: (state, action) => {
      const idx = state.posts.findIndex((el) => el.id === action.payload.id);
      state.posts[idx] = action.payload;
    },
  },
});

export const {
  setPosts,
  appendPost,
  modifyPost,
  removePost,
} = postSlice.actions;

export const selectPost = (state) => state.post;

export default postSlice.reducer;
