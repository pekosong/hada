import { createSlice } from "@reduxjs/toolkit";
import { changeProfile } from "../services/auth";
import { removeData } from "@modules/util";

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    token: null,
    profile: null,
    loading: false,
    err: null,
  },
  reducers: {
    setLogin: (state) => {
      state.isLogin = true;
    },
    setProfile: (state, action) => {
      state.profile = action.payload.profile;
      state.token = action.payload.token;
      state.err = null;
      state.isLogin = true;
    },

    updateProfile: (state, action) => {
      const { name, value } = action.payload;
      state.profile = { ...state.profile, [name]: value };
    },
    updateProfileStart: (state) => {
      state.loading = true;
      state.err = null;
    },
    updateProfileSuccess: (state, action) => {
      state.profile = action.payload;
      state.loading = false;
      state.err = null;
    },
    updateProfileError: (state) => {
      state.loading = false;
      state.err = "error";
    },
    setLogout: (state) => {
      removeData("token");
      removeData("userId");
      removeData("deviceToken");
      removeData("address");
      removeData("location");
      state.profile = null;
      state.token = null;
      state.isLogin = false;
    },
  },
});

export const {
  setLogin,
  setProfile,
  updateProfile,
  updateProfileStart,
  updateProfileSuccess,
  updateProfileError,
  setLogout,
} = authSlice.actions;

export const updateProfileAsync = ({ profile }) => async (dispatch) => {
  dispatch(updateProfileStart());
  try {
    const { data } = await changeProfile(profile);
    dispatch(updateProfileSuccess(data));
  } catch (err) {
    dispatch(updateProfileError());
  }
};

export const selectAuth = (state) => state.auth;

export default authSlice.reducer;
