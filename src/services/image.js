import axios from "axios";

const uploadImage = async (image) => {
  try {
    const { data } = await axios.post("upload", image, {
      headers: {
        "Content-Type":
          "multipart/form-data;boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL",
      },
    });
    return data[0]["id"];
  } catch (err) {
    console.log(err.response.data);
  }
};

export { uploadImage };
