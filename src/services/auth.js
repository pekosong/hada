import axios from "axios";
import { getData } from "@modules/util";

const signIn = async (username) => {
  const result = await axios.post(`auth/local`, { identifier: username });
  return result;
};

const getProfile = async () => {
  const token = await getData("token");
  const result = await axios.get(`users/me`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const changeProfile = async (profile) => {
  const token = await getData("token");
  const result = await axios.put(`users/${profile.id}`, profile, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const setDeviceToken = async ({ id, deviceToken }) => {
  const token = await getData("token");
  await axios.put(
    `users/${id}`,
    { deviceToken },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

const verifyEmail = async () => {
  const result = await axios.get("posts");
  return result;
};

const verifyUsername = async (username) => {
  const result = await axios.get(`users/count?username=${username}`);
  return result;
};

const verifyNick = async (nick) => {
  const result = await axios.get(`users/count?nickname=${nick}`);
  return result;
};

const signUp = async (body) => {
  const result = await axios.post("auth/local/register", body);
  return result;
};

const getUsersByIds = async (ids) => {
  const token = await getData("token");
  const result = await axios.get(`users?${ids}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

export {
  verifyUsername,
  verifyEmail,
  verifyNick,
  setDeviceToken,
  signIn,
  getProfile,
  changeProfile,
  signUp,
  getUsersByIds,
};
