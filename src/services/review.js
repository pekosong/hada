import axios from "axios";
import { getData } from "@modules/util";

const isWriteReview = async ({ post, id }) => {
  const token = await getData("token");
  const result = await axios.get(`reviews?post=${post}&guest=${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const addReview = async ({ data }) => {
  const token = await getData("token");
  const result = await axios.post("reviews", data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const getUserReviews = async (id) => {
  const result = await axios.get(`reviews?host=${id}`);
  return result;
};

export { isWriteReview, addReview, getUserReviews };
