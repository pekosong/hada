import axios from "axios";
import { getData } from "@modules/util";

const getPostById = async (id) => {
  const token = await getData("token");
  const result = await axios.get(`posts/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const getPosts = async ({ sort, sortType, type }) => {
  let query = type ? `?type=${type}` : "?";

  if (sort) query = `${query}&_sort=${sort}:${sortType}`;
  const result = await axios.get("posts" + query);
  return result;
};

const addPayment = async ({ data }) => {
  const token = await getData("token");
  const result = await axios.post("payments", data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const changePayment = async ({ id, data }) => {
  const token = await getData("token");
  const result = await axios.put(`payments/${id}`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

export { getPostById, getPosts, addPayment, changePayment };
