import axios from "axios";
import { getData } from "@modules/util";

const getNotifications = async ({ user }) => {
  const result = await axios.get(
    `notifications?_sort=created_at:DESC&user=${user}`
  );
  return result;
};

const setNotificationsRead = async (ids) => {
  const token = await getData("token");
  const promisses = ids.map((el) => {
    axios.put(
      `notifications/${el}`,
      { see: true },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  });
  await Promise.all(promisses);
};

const removeAllNotifications = async (ids) => {
  const token = await getData("token");
  const promisses = ids.map((el) => {
    axios.delete(`notifications/${el}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  });
  await Promise.all(promisses);
};

export { getNotifications, setNotificationsRead, removeAllNotifications };
