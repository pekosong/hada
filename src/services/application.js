import axios from "axios";
import { getData } from "@modules/util";

const getMyApplications = async () => {
  const token = await getData("token");
  const userId = await getData("userId");
  const { data } = await axios.get(`applications?user=${userId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
};

const getPostApplications = async (postId) => {
  const token = await getData("token");
  const { data } = await axios.get(`applications?post=${postId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
};

const addApplication = async (data) => {
  const token = await getData("token");
  try {
    const result = await axios.post(`applications`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return result;
  } catch (err) {
    return err.response.data;
  }
};

const removeApplication = async (id) => {
  const token = await getData("token");
  const result = await axios.delete(`applications/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const updateApplication = async ({ id, status }) => {
  const token = await getData("token");
  const result = await axios.put(
    `applications/${id}`,
    { status },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return result;
};

export {
  getMyApplications,
  getPostApplications,
  addApplication,
  removeApplication,
  updateApplication,
};
