import axios from "axios";
import { getData } from "@modules/util";

const getMyPosts = async () => {
  const token = await getData("token");
  const userId = await getData("userId");
  const { data } = await axios.get(`posts?user=${userId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
};

const getPostById = async (id) => {
  const token = await getData("token");
  const result = await axios.get(`posts/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const removePostById = async (id) => {
  const token = await getData("token");
  const result = await axios.delete(`posts/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const getPosts = async ({ sort, sortType, type }) => {
  let query = type ? `?type=${type}` : "?";
  if (sort) query = `${query}&_sort=${sort}:${sortType}`;
  const result = await axios.get("posts" + query);
  return result;
};

const getPostsByKeyword = async ({ offset, type, keyword }) => {
  let query = `_start=${offset}&title_contains=${keyword}&type=${type}`;
  const result = await axios.get("posts?" + query);
  return result;
};

const getPostsByDistance = async ({ offset, type, sortType, filterType }) => {
  const location = await getData("location");
  if (!location) return { data: [] };
  const { latitude, longitude } = location;
  let query = `lat=${latitude}&lng=${longitude}&offset=${offset}&type=${type}`;
  if (sortType) {
    const sortValue = sortType.split("_");
    query += `&${sortValue[0]}=${sortValue[1]}`;
  }
  if (filterType && filterType.split("_")[1] !== "all") {
    const filterValue = filterType.split("_");
    query += `&orderType=${filterValue[0]}&typeValue=${filterValue[1]}`;
  }
  const result = await axios.get("posts/findDistance?" + query);
  return result;
};

const getUserPosts = async (id) => {
  const result = await axios.get(`posts?user=${id}`);
  return result;
};

const addPost = async ({ data }) => {
  const token = await getData("token");
  const result = await axios.post("posts", data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

const updatePost = async ({ id, data }) => {
  const token = await getData("token");
  const result = await axios.put(`posts/${id}`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return result;
};

export {
  getMyPosts,
  getPostById,
  removePostById,
  getPosts,
  getPostsByKeyword,
  getPostsByDistance,
  getUserPosts,
  addPost,
  updatePost,
};
