module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["./"],
          alias: {
            "@components": "./src/components",
            "@screens": "./src/screens",
            "@contexts": "./src/contexts",
            "@modules": "./src/modules",
            "@hooks": "./src/hooks",
            "@services": "./src/services",
            "@stores": "./src/stores",
            "@constants": "./src/constants",
            "@navigation": "./src/navigation",
            "@assets": "./assets",
            "@layouts": "./src/layouts",
          },
        },
      ],
    ],
  };
};
