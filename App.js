import React from "react";
import Navigation from "./src/navigation/IndexNavigation";

import { Provider } from "react-redux";
import store from "./src/stores";
import { SafeAreaProvider } from "react-native-safe-area-context";
import axios from "axios";
import * as SplashScreen from "expo-splash-screen";

// SplashScreen.preventAutoHideAsync()
//   .then()
//   .catch((err) => {});

axios.defaults.baseURL = "https://hadawork.com";

export default function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <Navigation />
      </Provider>
    </SafeAreaProvider>
  );
}
